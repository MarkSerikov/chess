$(document).ready(function() {
	
	/*var $startEl = $(".start"),
	$dragEl = $(".drag"),
	$stopEl = $(".stop");
	
	Для счетчика
	var counts = [0, 0, 0];
	
	$( "span[class^='shape']" ).draggable({ 
		snap:".cell_field", 
		
		tart: function(){
			$($(this), $startEl).text("stert");
		},
		drag: function(){
			$($(this), $dragEl).text("drag");
		},
		stop: function(){
			$($(this), $stopEl).text("stop");
		}
	});*/
	
	//Обработка авторизации
	$("#autorisationForm").submit(function() {
		var name = $("input#name").val();
		var passwd = $("input#passwd").val();
		var user = new Object();
		user["name"] = name;
		user["password"] = passwd;

		$.ajax({
			type : "POST",
			url : "/chess/autorisation",
			data : "user=" + JSON.stringify(user),
			cache : false,
			dataType : "json",
			success : function(data) {
				$("div#menu").html(generationMenu());
				$("div#content").html("Hello user");
				
				$(".you_come_how").show();
				$(".login").html(data.name);
				$(".password").html(data.password);
				$(".id").html(data.id);
				
				clickOnLink();
			}
		});
		return false;
	});
});

function checkMsg() {
	
	var color = $("span.gameColor").text();
	
	$.ajax({
		type : "POST",
		url : "/chess/findStep",
		data : "color=" + color,
		cache : false,
		dataType : "json",
		success : function(data) {
			/*alert(data.step);*/
			
			if(data.rock) {
				if(data.position == "left") {
					if(color == "white") {
						var king = $("td#E8").html();
						$("td#E8").html("");
						$("td#C8").html(king);
						var turm = $("td#A8").html();
						$("td#E8").html("");
						$("td#D8").html(turm);
					} else if(color == "black") {
						var king = $("td#E1").html();
						$("td#E1").html("");
						$("td#C1").html(king);
						var turm = $("td#A1").html();
						$("td#E1").html("");
						$("td#D1").html(turm);
					}
				} else if(data.position == "right") {
					if(color == "white") {
						var king = $("td#E8").html();
						$("td#E8").html("");
						$("td#G8").html(king);
						var turm = $("td#H8").html();
						$("td#H8").html("");
						$("td#F8").html(turm);
					} else if(color == "black") {
						var king = $("td#E1").html();
						$("td#E1").html("");
						$("td#G1").html(king);
						var turm = $("td#H1").html();
						$("td#H1").html("");
						$("td#F1").html(turm);
					}
				}
				
			} else {
				var step = data.step.split("-");
				var from = step[0];
				var to = step[1];
				var shape = $("td#"+from).html();
				$("td#"+from).html("");
				$("td#"+to).html(shape);
				if (data.corner) {
					aler(123);
					var next = $("td#"+data.cord).next();
					next.html("");
				}
				if(data.mat) {
					alert("Вам мат");
				} else if(data.check) {
					alert("Вам шаг");
				}
			}
		},
		error : function(data) {
		}
	});
	setTimeout("checkMsg()",1000); 
};

// Обработка формы для регистрации ****************************************************
function registration() {
	$("div#content").html(generationRegistrationForm());
	// Обработка авторизации
	$("#registrationForm").submit(function() {
		var name = $("input#name").val();
		var passwd = $("input#passwd").val();
		var email = $("input#email").val();

		var user = new Object();

		user["name"] = name;
		user["password"] = passwd;
		user["email"] = email;
		
		alert(user["name"]+
		user["password"]+
		user["email"]);

		$.ajax({
			type : "POST",
			url : "/chess/registration",
			data : "user=" + JSON.stringify(user),
			cache : false,
			dataType : "json",
			success : function(data) {
				$("div#menu").html(generationMenu());
				$("div#content").html("Поздравляем с успешной регистрацией");
				var userExists = data.user;
				$(".you_come_how").show();
				$("span.login").html(userExists.login);
				$("span.password").html(userExists.password);
				$("span.id").html(userExists.id);
				
				clickOnLink();
			}
		});
		return false;
	});
	return false;
}

function generationRegistrationForm() {
	var regForm = "";
	regForm += '<form id="registrationForm" method="post">';
	regForm += 		'<label for="name"></label>';
	regForm += 		'<input type="text" name="login" id="name" />';
	regForm += 		'<label for="passwd"></label>';
	regForm += 		'<input type="password" name="password" id="passwd" />';
	regForm += 		'<label for="email"></label> <input type="text" name="email" id="email" />';
	regForm += 		'<br/>';
	regForm += 		'<input type="reset" value="Clear" class="btn btn-info btn-small"/> ';
	regForm += 		'<input type="submit" value="Зарегистрироваться" class="btn btn-success"/>';
	regForm += '</form>';
	return regForm;
}
///////////////////////////////////////////////////////////////////////////////////////////////

function generationMenu() {
	var menu = "";
	menu += '<div id="chess-menu">';
	menu += 	'<div class="navbar">';
	menu += 		'<div class="navbar-inner">';
	menu += 			'<a class="brand" href="#">Chess online</a>';
	menu += 			'<ul class="nav">';
	menu += 				'<li class="active"><a href="/chess">Главная</a></li>';
	menu += 				'<li><a class="menu_link" id="new_game" href="#">Новая игра</a></li>';
//	menu +=					'<li><span onclick="chooseColor();" class="span_ahref_fake">Новая игра</span></li>';
//	menu +=					'<li><a href="/continue">Продолжить партию</a></li>';
	menu +=					'<li><a class="menu_link" id="info" href="#">О сайте</a></li>';
	menu +=				'</ul>';
	menu +=			'</div>';
	menu +=		'</div>';
	menu +=	'</div>';
	return menu;
};

function clickOnLink() {
	checkMsg();
	$("body").on("click", "a.menu_link", function() {
		var action = $(this).prop("id");
		if(action == "new_game") {
			chooseColor();
		}
		if(action == "info") {
			getInfo();
		}
	});
}

function chooseColor() {
	$("div#content").html(generateChooseColor());
	$("li.active").removeClass("active");
	$("#new_game").parent().addClass("active");
	
}
function generateChooseColor() {
	var choose = "";
	white = "white";
	black = "black";
	choose += 	'<span class="choose_title">Выберите цвет</span><br/><br/>';
	choose += 	'<span onclick="getGame(white);" class="link_fake">Белые</span>';
	choose += 	'<span onclick="getGame(black);" class="link_fake">Черные</span>';
	return choose;
}

function getGame(color) {
	var id = $("span.id").text();
	var login = $("span.login").text();
	var password = $("span.password").text();
	
	var user = new Object();
	user["id"] = id;
	user["name"] = login;
	user["password"] = password;
	
	alert(user["id"] +" "+ user["login"] +" "+user["password"]);
	// Если передано что-то отличное от заданных цветов устанавливаем цвет по умолчанию
	if(color != "white" && color != "black") {
		color = "white";
	}
	
	$.ajax({
		type : "POST",
		url : "/chess/createGame",
		data : "user=" + JSON.stringify(user) + "&color="+color ,
		cache : false,
		dataType : "html",
		success : function(data) {
//			location.href="/chess/getGame?game="+data.game+"&color="+data.color;
			$("div#content").html(data);
			doDragable($("span.gameColor").text());
			doDropable();
		},
		error: function() {
			alert("Не удалось создать игру, попробуйте еще раз");
		}
	});
	return true;
}

function getInfo() {
	$.ajax({
		type : "POST",
		url : "/chess/info",
		/*data : "user=" + JSON.stringify(user) + "&color="+color ,*/
		cache : false,
		dataType : "html",
		success : function(data) {
			$("div#content").html(data);
			$("li.active").removeClass("active");
			$("#info").parent().addClass("active");
		},
		error: function() {
			alert("Не удалось получить информацию о сайте");
		}
	});
	return false;
}

function doDragable(color) {
	var $startEl = $(".start"),
//	$dragEl = $(".drag"),
	$stopEl = $(".stop");
	
	/*Для счетчика
	var counts = [0, 0, 0];*/
	
//	var from = "";
//	var to = "";
	
//	var shape = new Object();
//	var tdFrom = new Object();
//	var tdTo = new Object();
	
	$("span[class^='shape_"+color+"']" ).draggable({ 
		snap:".cell_field", 

		start: function(){
			shape = $($(this), $startEl);
			tdFrom = shape.parent();
			from = tdFrom.prop("id");
			
//			$($(this), $startEl).on(function() {
//				alert("stert");
//				
//			});
		},
		drag: function(){
//			$($(this), $dragEl).text("drag");
		},
		stop: function(){
			/*tdTo = $($(this), $stopEl).parent();
			to = tdTo.prop("id");*/
			
//			$($(this), $stopEl).text("stop");
		}
	});
}

function doDropable() {
	$('.field').droppable({
        hoverClass: 'dropHere',
        drop: function(event, ui) {
            $(this).append($('<div class="textBlock">' + ui.draggable.html() + '</div>'));
        }
    });
	
	$(".field").droppable({
		  accept:".ui-draggable",
//		  activeClass:"ui-state-hover",
		  drop:function(event, ui){
//		    $(this).addClass("ui-state-highlight")
//		    .find("span")
//		    .html("Поймал!");
		    
//			ui.draggable.removeAttr("style");
			  
//		    $(this).html(ui.draggable);
			
			tdTo = $(this);
			to = tdTo.prop("id");
			
//			alert("from=" + from + " to=" + to);
			
			var id = $("span.id").text();
			var login = $("span.login").text();
			var password = $("span.password").text();
			
			var user = new Object();
			user["id"] = id;
			user["name"] = login;
			user["password"] = password;
			
			var color = $("span.gameColor").text();
		    
		    $.ajax({
				type : "POST",
				url : "/chess/step",
				data : "user=" + JSON.stringify(user) + "&from=" + from + "&to=" + to + "&color=" + color,
				cache : false,
				dataType : "json",
				success : function(data) {
					if(data.success) {
//						tdTo.addClass("ui-state-highlight").find("span");
						ui.draggable.removeAttr("style");
						tdTo.html(ui.draggable);
						if (data.corner) {
							aler(123);
							var next = $("td#"+data.cord).next();
							next.html("");
						}
						
						doDragable($("span.gameColor").text());
						if(data.rock) {
							if(data.position == "left") {
								if(color == "white") {
									var turm = $("td#A1").html();
									$("td#A1").html("");
									$("td#D1").html(turm);
								} else {
									var turm = $("td#A8").html();
									$("td#A8").html("");
									$("td#D8").html(turm);
								}
							} else if (data.position == "right") {
								if(color == "white") {
									var turm = $("td#H1").html();
									$("td#H1").html("");
									$("td#F1").html(turm);
								} else {
									var turm = $("td#H8").html();
									$("td#H8").html("");
									$("td#F8").html(turm);
								}
							}
						}
//						alert("all is good");
					} else {
						alert("wrong step");
						ui.draggable.removeAttr("style");
						tdFrom.html(shape);
						doDragable($("span.gameColor").text());
					}
				},
				error: function() {
					alert("Не удалось выполнить ходсс");
					ui.draggable.removeAttr("style");
					tdFrom.html(shape);
					doDragable($("span.gameColor").text());
				}
			});
			return false;
		  }
		});
}
