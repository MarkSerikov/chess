<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!-- <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
 --><link type="text/css"
	href="<c:url value="/resources/css/bootstrap.min.css"/>"
	rel="Stylesheet" />
<link type="text/css"
	href="<c:url value="/resources/css/jquery-ui-1.10.1.custom.css"/>"
	rel="Stylesheet" />
<link type="text/css" href="<c:url value="/resources/css/chess-style.css"/>"
	rel="Stylesheet" />
<script type="text/javascript"
	src="<c:url value="/resources/js/jquery-1.9.1.js"/>"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/jquery-ui-1.10.1.custom.js"/>"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/script.js"/>"></script>

<%-- <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Игра</title>
</head>
<body>

	<div class="container-fluid">

		<!-- Голова -->
		<div class="row-fluid" id="header">
			<div class="span4">
				<div id="header-span">mark-serikov.com</div>
			</div>
			<div class="span4">
				<div id="header-span" class="well">Шахматы онлайн</div>
			</div>
			<div class="span4">
				<div id="header-span">${color}</div>
			</div>
		</div>

		<!-- Туловище -->
		<div class="row-fluid" id="header" style="height: 900px;">
			<div class="span8">
				<div class="well">

					<div id="menu"></div>

					<div id="content">
 --%>					
 					<span class="gameColor">${color}</span>
					<table style="border: 1px solid;">
						<tbody>
							<c:forEach var="i" begin="1" end="10">
								
								<tr>
								
									<c:set var="temp" value="8"/>
									<c:forEach var="j" begin="1" end="10">
				
										<c:choose>
											
											<%--Отрисовываем границы поля --%>
											<c:when test="${(i == 1) or (i == 10) or (j == 1) or (j == 10)}">
												
												<%--Вывод цифр --%>
												<c:if test="${j == 1 or j == 10}">
													<c:if test="${i > 1 and i < 10}">
														<td class="borderDesc">
															<c:if test="${color eq 'white'}">
																${10 - i}
															</c:if>
															<c:if test="${color eq 'black'}">
																${i - 1}
															</c:if>
														</td>
													</c:if>
													<c:if test="${i == 1 or i == 10}">
														<td class="borderDesc"></td>
													</c:if>
												</c:if>
												
												<%--Вывод букв --%>
												<c:if test="${color eq 'white'}">
													<c:if test="${j == 2}"><td class="borderDesc">A</td></c:if>
													<c:if test="${j == 3}"><td class="borderDesc">B</td></c:if>
													<c:if test="${j == 4}"><td class="borderDesc">C</td></c:if>
													<c:if test="${j == 5}"><td class="borderDesc">D</td></c:if>
													<c:if test="${j == 6}"><td class="borderDesc">E</td></c:if>
													<c:if test="${j == 7}"><td class="borderDesc">F</td></c:if>
													<c:if test="${j == 8}"><td class="borderDesc">G</td></c:if>
													<c:if test="${j == 9}"><td class="borderDesc">H</td></c:if>
												</c:if>
												
												<c:if test="${color eq 'black'}">
													<c:if test="${j == 2}"><td class="borderDesc">H</td></c:if>
													<c:if test="${j == 3}"><td class="borderDesc">G</td></c:if>
													<c:if test="${j == 4}"><td class="borderDesc">F</td></c:if>
													<c:if test="${j == 5}"><td class="borderDesc">E</td></c:if>
													<c:if test="${j == 6}"><td class="borderDesc">D</td></c:if>
													<c:if test="${j == 7}"><td class="borderDesc">C</td></c:if>
													<c:if test="${j == 8}"><td class="borderDesc">B</td></c:if>
													<c:if test="${j == 9}"><td class="borderDesc">A</td></c:if>
												</c:if>
												
											</c:when>
											
											<%-- Поле --%>
											<c:otherwise>
											
												<td id="<c:if test="${color eq 'white'}">${charMapWhite.get(j)}</c:if><c:if test="${color eq 'black'}">${charMapBlack.get(j)}</c:if><c:if test="${color eq 'white'}">${10-i}</c:if><c:if test="${color eq 'black'}">${i-1}</c:if>" class='cell_field<c:if test="${((i%2==0 and j%2==1) or (i%2==1 and j%2==0)) and (i > 1 and j > 1) and (i < 10 and j < 10)}"> gray</c:if> field'>
												
													<%--Фигуры одного цвета --%>	
													<c:if test="${i == 2 and j == 2}">
														<span class="<c:if test="${color eq 'white'}">shape_black_turm</c:if><c:if test="${color eq 'black'}">shape_white_turm</c:if>"></span>
													</c:if>
													
													<c:if test="${i == 2 and j == 3}">
														<span class="<c:if test="${color eq 'white'}">shape_black_horse</c:if><c:if test="${color eq 'black'}">shape_white_horse</c:if>"></span>
													</c:if>
													
													<c:if test="${i == 2 and j == 4}">
														<span class="<c:if test="${color eq 'white'}">shape_black_elephant</c:if><c:if test="${color eq 'black'}">shape_white_elephant</c:if>"></span>
													</c:if>
													
													<c:if test="${i == 2 and j == 5}">
														<span class="<c:if test="${color eq 'white'}">shape_black_queen</c:if><c:if test="${color eq 'black'}">shape_white_king</c:if>"></span>
													</c:if>
													
													<c:if test="${i == 2 and j == 6}">
														<span class="<c:if test="${color eq 'white'}">shape_black_king</c:if><c:if test="${color eq 'black'}">shape_white_queen</c:if>"></span>
													</c:if>
													
													<c:if test="${i == 2 and j == 7}">
														<span class="<c:if test="${color eq 'white'}">shape_black_elephant</c:if><c:if test="${color eq 'black'}">shape_white_elephant</c:if>"></span>
													</c:if>
													
													<c:if test="${i == 2 and j == 8}">
														<span class="<c:if test="${color eq 'white'}">shape_black_horse</c:if><c:if test="${color eq 'black'}">shape_white_horse</c:if>"></span>
													</c:if>
													
													<c:if test="${i == 2 and j == 9}">
														<span class="<c:if test="${color eq 'white'}">shape_black_turm</c:if><c:if test="${color eq 'black'}">shape_white_turm</c:if>"></span>
													</c:if>
													
													<c:if test="${i == 3 and (j > 1 and j < 10)}">
														<span class="<c:if test="${color eq 'white'}">shape_black_pawn</c:if><c:if test="${color eq 'black'}">shape_white_pawn</c:if>"></span>
													</c:if>
													
													
													<%--Фигуры другого цвета --%>	
													<c:if test="${i == 9 and j == 2}">
														<span class="<c:if test="${color eq 'white'}">shape_white_turm</c:if><c:if test="${color eq 'black'}">shape_black_turm</c:if>"></span>
													</c:if>
													
													<c:if test="${i == 9 and j == 3}">
														<span class="<c:if test="${color eq 'white'}">shape_white_horse</c:if><c:if test="${color eq 'black'}">shape_black_horse</c:if>"></span>
													</c:if>
													
													<c:if test="${i == 9 and j == 4}">
														<span class="<c:if test="${color eq 'white'}">shape_white_elephant</c:if><c:if test="${color eq 'black'}">shape_black_elephant</c:if>"></span>
													</c:if>
													
													<c:if test="${i == 9 and j == 5}">
														<span class="<c:if test="${color eq 'white'}">shape_white_queen</c:if><c:if test="${color eq 'black'}">shape_black_king</c:if>"></span>
													</c:if>
													
													<c:if test="${i == 9 and j == 6}">
														<span class="<c:if test="${color eq 'white'}">shape_white_king</c:if><c:if test="${color eq 'black'}">shape_black_queen</c:if>"></span>
													</c:if>
													
													<c:if test="${i == 9 and j == 7}">
														<span class="<c:if test="${color eq 'white'}">shape_white_elephant</c:if><c:if test="${color eq 'black'}">shape_black_elephant</c:if>"></span>
													</c:if>
													
													<c:if test="${i == 9 and j == 8}">
														<span class="<c:if test="${color eq 'white'}">shape_white_horse</c:if><c:if test="${color eq 'black'}">shape_black_horse</c:if>"></span>
													</c:if>
													
													<c:if test="${i == 9 and j == 9}">
														<span class="<c:if test="${color eq 'white'}">shape_white_turm</c:if><c:if test="${color eq 'black'}">shape_black_turm</c:if>"></span>
													</c:if>
													
													<c:if test="${i == 8 and (j > 1 and j < 10)}">
														<span class="<c:if test="${color eq 'white'}">shape_white_pawn</c:if><c:if test="${color eq 'black'}">shape_black_pawn</c:if>"></span>
													</c:if>
													
													<c:if test="${i > 3 && i < 8 and (j > 1 and j < 10)}"><span></span></c:if>
												
												</td>
															
											</c:otherwise>
										
										</c:choose>
								
									</c:forEach>
								
								</tr>
								
							</c:forEach>
						</tbody>
					</table>

<!-- 					</div>

				</div>
			</div>
			<div class="span4">
				<div class="well">Информация о ходах</div>
			</div>
		</div>

		Подвал
		<div class="row-fluid" id="footer">
			<div class="span12">
				<div class="well">Подвал</div>
			</div>
		</div>

	</div>

</body>
</html> -->