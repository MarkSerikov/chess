<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>

<head>
<link type="text/css"
	href="<c:url value="/resources/css/bootstrap.min.css"/>"
	rel="Stylesheet" />
<link type="text/css"
	href="<c:url value="/resources/css/jquery-ui-1.10.1.custom.css"/>"
	rel="Stylesheet" />
<link type="text/css" href="<c:url value="/resources/css/chess-style.css"/>"
	rel="Stylesheet" />
<script type="text/javascript"
	src="<c:url value="/resources/js/jquery-1.9.1.js"/>"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/jquery-ui-1.10.1.custom.js"/>"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/script.js"/>"></script>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Добро пожаловать</title>
</head>
<body>

	<div class="container-fluid">

		<!-- Голова -->
		<div class="row-fluid" id="header">
			<div class="span4">
				<div id="header-span">Mark-Serikov.com</div>
			</div>
			<div class="span4" style="vertical-align: middle;">
				<div id="header-span">Шахматы Онлайн</div>
			</div>
			<div class="span4">
				<div id="header-span">
					<div class="contactInfo">
						<span class="you_come_how">Вы вошли как:</span>
						<span class="login"></span>
						<span class="password"></span>
						<span class="id"></span>
					</div>
				</div>
			</div>
		</div>

		<!-- Туловище -->
		<div class="row-fluid" id="header" style="height: 900px;">
			<div class="span8">
				<div class="well">
				
					<div class="menu_link"></div>

					<div id="menu"></div>
					
					<div id="content">

						<form id="autorisationForm" method="post">
							<label for="name">Введите логин</label>
							<input type="text" name="login" id="name" />
							
							<label for="passwd">Введите пароль</label>
							<input type="password" name="password" id="passwd" />
							
							<br/>
							
							<div class="btn btn-info btn-small" onclick="registration();">Зарегестироваться</div>
							<input type="submit" value="Войти" class="btn btn-success"/>
						</form>
					
					</div>

				</div>
			</div>
			<div class="span4">
				<div class="well">История ходов</div>
			</div>
		</div>

		<!-- Подвал -->
		<div class="row-fluid" id="footer">
			<div class="span12">
				<div class="well">Подвал</div>
			</div>
		</div>

	</div>

</body>
</html>