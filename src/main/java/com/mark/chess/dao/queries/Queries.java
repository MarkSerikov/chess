package com.mark.chess.dao.queries;

public class Queries {

	public static final String SELECT_ID_USER = 
			"SELECT id_user FROM users WHERE login = :login AND password = :password";
	
	public static final String SAVE_NEW_USER =
			"INSERT INTO users(login, password, email) VALUES(:login, :password, :email)";

}
