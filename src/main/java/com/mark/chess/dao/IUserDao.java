package com.mark.chess.dao;

import com.mark.chess.model.User;

public interface IUserDao {

	/**
	 * Авторизация пользователя
	 * 
	 * @param user
	 * 
	 * @return id пользователя
	 */
	long getUserIdByLoginAndPassword(User user);

	/**
	 * ДОбавление нового пользователя
	 * 
	 * @param user
	 * 
	 * @return id вновь созданного пользователя
	 */
	long saveNewUser(User user);

}
