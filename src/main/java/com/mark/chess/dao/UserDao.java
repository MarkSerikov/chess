package com.mark.chess.dao;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import com.mark.chess.dao.queries.Queries;
import com.mark.chess.dao.rowmapper.UserIdRowMapper;
import com.mark.chess.model.User;

@Repository
public class UserDao implements IUserDao {

	@Autowired
	private DataSource dataSource;

	@Override
	public long getUserIdByLoginAndPassword(User user) {

		NamedParameterJdbcTemplate template = new NamedParameterJdbcTemplate(dataSource);
		
		MapSqlParameterSource parameterSourcec = new MapSqlParameterSource();
		parameterSourcec.addValue("login", user.getName());
		parameterSourcec.addValue("password", user.getPassword());

		List<Long> ids = template.query(
				Queries.SELECT_ID_USER, parameterSourcec,
				new UserIdRowMapper());
		if(!ids.isEmpty()) {
			return ids.get(0);
		}
		return 0;
		
	}

	@Override
	public long saveNewUser(User user) {
		
		NamedParameterJdbcTemplate namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(
				dataSource);
		MapSqlParameterSource parameterSourcec = new MapSqlParameterSource();
		parameterSourcec.addValue("login", user.getName());
		parameterSourcec.addValue("password", user.getPassword());
		parameterSourcec.addValue("email", user.getEmail());

		KeyHolder keyHolder = new GeneratedKeyHolder();
		namedParameterJdbcTemplate.update(Queries.SAVE_NEW_USER,
				parameterSourcec, keyHolder);
		
		Long id = (Long) keyHolder.getKeys().get("id_user");
		return id;
	}

}
