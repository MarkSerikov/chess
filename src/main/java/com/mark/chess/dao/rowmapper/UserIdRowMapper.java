package com.mark.chess.dao.rowmapper;

import java.sql.ResultSet;

import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class UserIdRowMapper implements RowMapper<Long> {

	@Override
	public Long mapRow(ResultSet rs, int ind) throws SQLException {
		return rs.getLong("id_user");
	}

}
