package com.mark.chess.service;

import com.mark.chess.model.User;

public interface IUserService {

	/**
	 * Авторизация пользователя
	 * 
	 * @param user
	 */
	long autorise(User user);

	/**
	 * Регистрация пользователя
	 * 
	 * @param user
	 */
	long registration(User user);

}
