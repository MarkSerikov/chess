package com.mark.chess.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mark.chess.dao.IUserDao;

import com.mark.chess.model.User;

@Service
public class UserService implements IUserService {

	@Autowired
	private IUserDao userDao;

	@Override
	public long autorise(User user) {

		long id = 0;
		if (user != null && !user.getName().isEmpty()
				&& !user.getPassword().isEmpty()) {
			
			user.setPassword(MD5(user.getPassword()));
			id = userDao.getUserIdByLoginAndPassword(user);
		}
		return id;
	}

	@Override
	public long registration(User user) {
		long id = 0;
		if (user != null && !user.getName().isEmpty()
				&& !user.getPassword().isEmpty()) {
			user.setPassword(MD5(user.getPassword()));
			id = userDao.saveNewUser(user);
		}
		return id;
	}

	
	
	/*Получение md5 хеш суммы*/
	private static String MD5(String md5) {
		try {
			java.security.MessageDigest md = java.security.MessageDigest
					.getInstance("MD5");
			byte[] array = md.digest(md5.getBytes());
			StringBuffer sb = new StringBuffer();
			for (int i = 0; i < array.length; ++i) {
				sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100)
						.substring(1, 3));
			}
			return sb.toString();
		} catch (java.security.NoSuchAlgorithmException e) {
		}
		return null;
	}
	
	public static void main(String[] args) {
		System.out.println(MD5("qwerty"));
	}

}
