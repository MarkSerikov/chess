package com.mark.chess.game;

public class GameForGamer {
	
	private Game game;
	private String color;
	private Gamer iAm;
	private Field field;
	
	public Gamer getiAm() {
		return iAm;
	}
	public void setiAm(Gamer iAm) {
		this.iAm = iAm;
	}
	public Field getField() {
		return field;
	}
	public void setField(Field field) {
		this.field = field;
	}
	
	public Game getGame() {
		return game;
	}
	public void setGame(Game game) {
		this.game = game;
	}
	
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
}
