package com.mark.chess.game;

import java.util.LinkedList;
import java.util.Queue;

public class GameQueue {
	
	private GameQueue() {}
	
	private static volatile Queue<Gamer> gameQueue;
	
	public static Queue<Gamer> getInstance() {
		if(gameQueue == null) {
			synchronized (GameQueue.class) {
				if(gameQueue == null) {
					gameQueue = new LinkedList<Gamer>();
				}
			}
		}
		return gameQueue;
	}

}
