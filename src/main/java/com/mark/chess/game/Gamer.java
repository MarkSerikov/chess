package com.mark.chess.game;

import com.mark.chess.model.User;

public class Gamer {
	
	private User user;
	private String color;
	private Game game;
	
	public Gamer(User user, String color) {
		this.user = user;
		this.color = color;
	}
	
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}

	public void setGame(Game game) {
		this.game = game;
	}
	public Game getGame() {
		return game;
	}
	
	

}
