package com.mark.chess.game.shape;

public class Step {
	
	private final boolean success;
	private final Shape beatShape;
	private String step;
	private boolean rock;
	private String color;
	private boolean check;
	private boolean mat;
	private String position;
	private boolean corner;
	private String cord;
	
	public void setRock(boolean rock) {
		this.rock = rock;
	}
	public boolean isCorner() {
		return corner;
	}
	public void setCorner(boolean corner) {
		this.corner = corner;
	}
	public void setCord(String cord) {
		this.cord = cord;
	}
	public String getCord() {
		return cord;
	}
	
	public void setPosition(String position) {
		this.position = position;
	}
	public String getPosition() {
		return position;
	}
	
	public Step(boolean success, Shape shape, String step, String color) {
		this.success = success;
		beatShape = shape;
		this.step = step;
		this.color = color;
	}
	
	public Shape getBeatShape() {
		return beatShape;
	}
	public boolean isSuccess() {
		return success;
	}
	public String getStep() {
		return step;
	}
	public boolean isRock() {
		return rock;
	}
	public String getColor() {
		return color;
	}
	public boolean isCheck() {
		return check;
	}
	public void setCheck(boolean check) {
		this.check = check;
	}
	public boolean isMat() {
		return mat;
	}
	public void setMat(boolean mat) {
		this.mat = mat;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((color == null) ? 0 : color.hashCode());
		result = prime * result + ((step == null) ? 0 : step.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Step other = (Step) obj;
		if (color == null) {
			if (other.color != null)
				return false;
		} else if (!color.equals(other.color))
			return false;
		if (step == null) {
			if (other.step != null)
				return false;
		} else if (!step.equals(other.step))
			return false;
		return true;
	}
}
