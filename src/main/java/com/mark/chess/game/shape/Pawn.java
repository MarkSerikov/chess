package com.mark.chess.game.shape;

import java.util.LinkedList;

import org.apache.log4j.Logger;

import com.mark.chess.game.Field;

public class Pawn extends Shape {
	
	private final static Logger LOGGER = Logger.getLogger(Pawn.class);

	public Pawn(String name, String color) {
		super(name, color);
	}

	private boolean firstStep = true;
	private Shape[][] field;
	private Shape shapeFrom;
	private LinkedList<String> history;
	private Shape brokenShape;
	private Step step;
	private int xFrom;
	private int yFrom;
	private int xTo;
	private int yTo;
	private String from;
	private String to;
	private Field fieldObject;

	@Override
	public Step doStep(Field fieldObject, String from, String to) {

		this.fieldObject = fieldObject;
		this.from = from;
		this.to = to;
		xFrom = Field.CHAR_MAP.get(from.charAt(0));
		yFrom = Integer.parseInt(String.valueOf(from.charAt(1))) - 1;

		xTo = Field.CHAR_MAP.get(to.charAt(0));
		yTo = Integer.parseInt(String.valueOf(to.charAt(1))) - 1;

		field = fieldObject.getField();

		shapeFrom = field[yFrom][xFrom];
		history = fieldObject.getHistory();

		if (firstStep) {

			System.out.println("first step");

			if ("white".equals(shapeFrom.getColor())) {
				if (field[yFrom + 1][xFrom] == null && field[yFrom + 2][xFrom] == null && (yTo - yFrom < 3)
						&& xFrom == xTo && yFrom != yTo) {

					moveShape(fieldObject);
//					checkMat();

				} else if (field[yTo][xTo] != null && (yTo - yFrom) == 1 && (Math.abs(xTo - xFrom) == 1)) {

					moveAndHitShape(fieldObject);
//					checkMat();

				} else {
					step = new Step(false, null, from + "-" + to, getColor());
				}
			} else if ("black".equals(shapeFrom.getColor())) {
				if (field[yFrom - 1][xFrom] == null && field[yFrom - 2][xFrom] == null && (yFrom - yTo < 3)
						&& xFrom == xTo) {

					moveShape(fieldObject);
//					checkMat();

				} else if (field[yTo][xTo] != null && (yTo - yFrom) == -1 && (Math.abs(xTo - xFrom) == 1)) {

					moveAndHitShape(fieldObject);
//					checkMat();

				} else {
					step = new Step(false, null, from + "-" + to, getColor());
				}
			}

		} else {

			if ("white".equals(shapeFrom.getColor())) {
				if (field[yFrom + 1][xFrom] == null && (yTo - yFrom == 1) && xFrom == xTo) {

					moveShape(fieldObject);
//					checkMat();

				} else if (field[yTo][xTo] != null && (yTo - yFrom) == 1 && (Math.abs(xTo - xFrom) == 1)) {

					if (!field[yTo][xTo].getColor().equals(field[yFrom][xFrom].getColor())) {

						moveAndHitShape(fieldObject);
//						checkMat();
					} else {
						step = new Step(false, null, from + "-" + to, getColor());
					}

				} else if (field[yTo][xTo] == null && (yTo - yFrom) == 1 && (Math.abs(xTo - xFrom) == 1)) {

					int yLastFrom = Integer.parseInt(String.valueOf(history.getLast().charAt(1)));

					int xLastTo = Field.CHAR_MAP.get(history.getLast().charAt(3));
					int yLastTo = Integer.parseInt(String.valueOf(history.getLast().charAt(4)));

					System.out.println("yLastTo - yLastFrom  = " + (yLastTo - yLastFrom) + " yLastTo =" +yLastTo + " yTo=" +yTo );
					
					if ((yLastTo - yLastFrom == -2) && (yLastTo - yTo == 0) && (xLastTo == xTo)) {

						
						
						
//						moveAndHitShape(fieldObject);
//						checkMat();
						
						brokenShape = field[yTo-1][xTo];
						field[yTo][xTo] = field[yFrom][xFrom];
						field[yFrom][xFrom] = null;

						int yKing = 0;
						int xKing = 0;
						if ("white".equals(getColor())) {
							yKing = fieldObject.getWhiteKing().getY();
							xKing = fieldObject.getWhiteKing().getX();
						} else if ("black".equals(getColor())) {
							yKing = fieldObject.getBlackKing().getY();
							xKing = fieldObject.getBlackKing().getX();
						}

						if (!fieldObject.checkBeat(yKing, xKing, getColor())) {
							setNotCheck();
							checkAllShapeOnCheckOpossite();
							firstStep = false;
							history.add(from + "-" + to);
							step = new Step(true, brokenShape, from + "-" + to, getColor());
							step.setCorner(true);
							step.setCord(from);
							fieldObject.getSteps().add(step);
						} else {
							field[yTo][xTo] = null;
							field[yFrom][xFrom] = shapeFrom;
							brokenShape = null;
							step = new Step(false, null, from + "-" + to, getColor());
						}
					}
				} else {
					step = new Step(false, null, from + "-" + to, getColor());
				}

			} else if ("black".equals(shapeFrom.getColor())) {
				if (field[yFrom - 1][xFrom] == null && (yFrom - yTo == 1) && xFrom == xTo) {

					moveShape(fieldObject);
//					checkMat();

				} else if (field[yTo][xTo] != null && (yTo - yFrom) == -1 && (Math.abs(xTo - xFrom) == 1)) {

					if (!field[yTo][xTo].getColor().equals(field[yFrom][xFrom].getColor())) {

						moveAndHitShape(fieldObject);
//						checkMat();

					} else {
						step = new Step(false, null, from + "-" + to, getColor());
					}

				} else if (field[yTo][xTo] == null && (yTo - yFrom) == -1 && (Math.abs(xTo - xFrom) == 1)) {

					int yLastFrom = Integer.parseInt(String.valueOf(history.getLast().charAt(1)));

					int xLastTo = Field.CHAR_MAP.get(history.getLast().charAt(3));
					int yLastTo = Integer.parseInt(String.valueOf(history.getLast().charAt(4)));

					if ((yLastTo - yLastFrom == 2) && (yLastTo - yTo == 0) && (xLastTo == xTo)) {

						moveAndHitShape(fieldObject);
//						checkMat();
						
						brokenShape = field[yTo-1][xTo];
						field[yTo][xTo] = field[yFrom][xFrom];
						field[yFrom][xFrom] = null;

						int yKing = 0;
						int xKing = 0;
						if ("white".equals(getColor())) {
							yKing = fieldObject.getWhiteKing().getY();
							xKing = fieldObject.getWhiteKing().getX();
						} else if ("black".equals(getColor())) {
							yKing = fieldObject.getBlackKing().getY();
							xKing = fieldObject.getBlackKing().getX();
						}

						if (!fieldObject.checkBeat(yKing, xKing, getColor())) {
							setNotCheck();
							checkAllShapeOnCheckOpossite();
							firstStep = false;
							step = new Step(true, brokenShape, from + "-" + to, getColor());
							step.setCorner(true);
							step.setCord(from);
							history.add(from + "-" + to);
							fieldObject.getSteps().add(step);
						} else {
							field[yTo][xTo] = null;
							field[yFrom][xFrom] = shapeFrom;
							brokenShape = null;
							step = new Step(false, null, from + "-" + to, getColor());
						}

					}

				} else {
					step = new Step(false, null, from + "-" + to, getColor());
				}
			}
		}
		return step;
	}

	private void moveAndHitShape(Field fieldObject) {
		brokenShape = field[yTo][xTo];
		field[yFrom][xFrom] = null;
		field[yTo][xTo] = shapeFrom;

		int yKing = 0;
		int xKing = 0;
		if ("white".equals(getColor())) {
			yKing = fieldObject.getWhiteKing().getY();
			xKing = fieldObject.getWhiteKing().getX();
		} else if ("black".equals(getColor())) {
			yKing = fieldObject.getBlackKing().getY();
			xKing = fieldObject.getBlackKing().getX();
		}

		if (!fieldObject.checkBeat(yKing, xKing, getColor())) {
			setNotCheck();
			checkAllShapeOnCheckOpossite();
			firstStep = false;
			history.add(from + "-" + to);
			step = new Step(true, brokenShape, from + "-" + to, getColor());
			fieldObject.getSteps().add(step);
		} else {
			field[yTo][xTo] = null;
			field[yFrom][xFrom] = shapeFrom;
			brokenShape = field[yTo][xTo];
			step = new Step(false, null, from + "-" + to, getColor());
		}
	}

	private void moveShape(Field fieldObject) {
		field[yFrom][xFrom] = null;
		field[yTo][xTo] = shapeFrom;

		int yKing = 0;
		int xKing = 0;
		if ("white".equals(getColor())) {
			yKing = fieldObject.getWhiteKing().getY();
			xKing = fieldObject.getWhiteKing().getX();
		} else if ("black".equals(getColor())) {
			yKing = fieldObject.getBlackKing().getY();
			xKing = fieldObject.getBlackKing().getX();
		}

		if (!fieldObject.checkBeat(yKing, xKing, getColor())) {
			setNotCheck();
			checkAllShapeOnCheckOpossite();
			firstStep = false;
			history.add(from + "-" + to);
			step = new Step(true, null, from + "-" + to, getColor());
			fieldObject.getSteps().add(step);
		} else {
			field[yFrom][xFrom] = shapeFrom;
			field[yTo][xTo] = null;
			step = new Step(false, null, from + "-" + to, getColor());
		}
	}

	private void setNotCheck() {
		if ("white".equals(getColor())) {
			fieldObject.getWhiteKing().setCheck(false);
		} else if ("black".equals(getColor())) {
			fieldObject.getBlackKing().setCheck(false);
		}
	}

	@Override
	public boolean checkBeatKing(int y, int x, int yKing, int xKing, Shape[][] shapes) {

		if (Math.abs(yKing - y) == 1) {
			if (x == 0) {
				if (x + 1 == xKing) {
					return true;
				}
			} else if (x == 7) {
				if (x - 1 == xKing) {
					return true;
				}
			} else if (x > 0 && x < 7) {
				if (x - 1 == xKing || x + 1 == xKing) {
					return true;
				}
			}
		}
		return false;
	}

	// проверка, после хода фигуры, не бъет ли союзная фигура короля
	private void checkAllShapeOnCheckOpossite() {
		String color = "";
		int yKing = 0;
		int xKing = 0;
		if ("white".equals(getColor())) {
			color = "black";
			yKing = fieldObject.getBlackKing().getY();
			xKing = fieldObject.getBlackKing().getX();
		} else if ("black".equals(getColor())) {
			color = "white";
			yKing = fieldObject.getWhiteKing().getY();
			xKing = fieldObject.getWhiteKing().getX();
		}

		if (fieldObject.checkBeat(yKing, xKing, color)) {
			if (!"white".equals(getColor())) {
				System.out.println("BEAT WHITE KING");
				fieldObject.getWhiteKing().setCheck(true);
			} else if (!"black".equals(getColor())) {
				System.out.println("BEAT BLACK KING");
				fieldObject.getBlackKing().setCheck(true);
			}
		}
	}

	private void checkMat() {
		int yKing = 0;
		int xKing = 0;
		if (!"white".equals(getColor())) {
			yKing = fieldObject.getWhiteKing().getY();
			xKing = fieldObject.getWhiteKing().getX();
		} else if (!"black".equals(getColor())) {
			yKing = fieldObject.getBlackKing().getY();
			xKing = fieldObject.getBlackKing().getX();
		}

		if (this.checkBeatKing(yTo, xTo, yKing, xKing, fieldObject.getField())) {

			if (!"white".equals(getColor())) {
				System.out.println("BEAT WHITE KING");
				fieldObject.getWhiteKing().setCheck(true);
			} else if (!"black".equals(getColor())) {
				System.out.println("BEAT BLACK KING");
				fieldObject.getBlackKing().setCheck(true);
			}

			if (((King) fieldObject.getField()[yKing][xKing]).isMat(yKing, xKing, fieldObject, yTo, xTo)) {
				if (!"white".equals(getColor())) {
					fieldObject.getWhiteKing().setMat(true);
				} else if (!"black".equals(getColor())) {
					fieldObject.getBlackKing().setMat(true);
				}
			}
		}
	}

	@Override
	public boolean opportunityToClose(int yKing, int xKing, int y, int x) {
		if(fieldObject.checkBlock(y, x, getColor())) {
			return true;
		}
		return false;
	}

	@Override
	public boolean isCanStep(int y, int x, int yTemp, int xTemp, Shape[][] field) {
		
		if(firstStep) {
			if(x == 0) {
				if((x == xTemp) && (("white".equals(getColor()) ? y - yTemp == -2 : y - yTemp == 2) || ("white".equals(getColor()) ? y - yTemp == -1 : y - yTemp == 1))) {
					System.out.println("PAWN IS BLOCK 1");
					return true;
				}
			} else if (x == 7) {
				if((x == xTemp) && (("white".equals(getColor()) ? y - yTemp == -2 : y - yTemp == 2) || ("white".equals(getColor()) ? y - yTemp == -1 : y - yTemp == 1))) {
					System.out.println("PAWN IS BLOCK 2");
					return true;
				}
			} else if (x < 7 && x > 0) {
				if((x == xTemp) && (("white".equals(getColor()) ? y - yTemp == -2 : y - yTemp == 2) || ("white".equals(getColor()) ? y - yTemp == -1 : y - yTemp == 1))) {
					System.out.println("PAWN IS BLOCK 3");
					return true;
				}
			}
		} else {
			if(x == 0) {
				if((x == xTemp) && ("white".equals(getColor()) ? y - yTemp == -1 : y - yTemp == 1)) {
					System.out.println("PAWN IS BLOCK 4");
					return true;
				}
			} else if (x == 7) {
				if((x == xTemp) && ("white".equals(getColor()) ? y - yTemp == -1 : y - yTemp == 1)) {
					System.out.println("PAWN IS BLOCK 5");
					return true;
				}
			} else if (x < 7 && x > 0) {
				if((x == xTemp) && ("white".equals(getColor()) ? y - yTemp == -1 : y - yTemp == 1)) {
					System.out.println("y="+y+" x="+x);
					System.out.println("yTemp="+yTemp+" xTemp="+xTemp);
					System.out.println("PAWN IS BLOCK 6");
					return true;
				}
			}
		}
		return false;
	}
}
