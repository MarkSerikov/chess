package com.mark.chess.game.shape;

import java.util.LinkedList;

import org.apache.log4j.Logger;

import com.mark.chess.game.Field;

public class Elephant extends Shape {
	
	private final static Logger LOGGER = Logger.getLogger(Elephant.class);

	public Elephant(String name, String color) {
		super(name, color);
	}

	private Shape[][] field;
	private Shape shapeFrom;
	private LinkedList<String> history;
	private Shape brokenShape;
	private Step step;
	private int xFrom;
	private int yFrom;
	private int xTo;
	private int yTo;
	private Field fieldObject;

	@Override
	public Step doStep(Field fieldObject, String from, String to) {

		LOGGER.debug("Elephant do step");
		
		this.fieldObject = fieldObject;
		xFrom = Field.CHAR_MAP.get(from.charAt(0));
		yFrom = Integer.parseInt(String.valueOf(from.charAt(1))) - 1;

		xTo = Field.CHAR_MAP.get(to.charAt(0));
		yTo = Integer.parseInt(String.valueOf(to.charAt(1))) - 1;

		field = fieldObject.getField();

		shapeFrom = field[yFrom][xFrom];
		history = fieldObject.getHistory();

		int defY = yTo - yFrom;
		int defX = xTo - xFrom;

		int yTemp = yTo;
		int xTemp = xTo;

		boolean error = false;

		if (Math.abs(defY) == Math.abs(defX)) {
			
			LOGGER.debug("Elephant start check step");

			// есть фигура
			if (field[yTo][xTo] != null && !field[yTo][xTo].getColor().equals(field[yFrom][xFrom].getColor())) {
				while (((defY > 0 ? --yTemp : ++yTemp) != yFrom) && (defX < 0 ? ++xTemp : --xTemp) != xFrom) {

					if (field[yTemp][xTemp] != null) {
						LOGGER.error("Elephant can't step");
						error = true;
						break;
					}
				}
				if (!error) {

					field[yFrom][xFrom] = null;
					brokenShape = field[yTo][xTo];
					field[yTo][xTo] = shapeFrom;

					int yKing = 0;
					int xKing = 0;
					if ("white".equals(getColor())) {
						yKing = fieldObject.getWhiteKing().getY();
						xKing = fieldObject.getWhiteKing().getX();
					} else if ("black".equals(getColor())) {
						yKing = fieldObject.getBlackKing().getY();
						xKing = fieldObject.getBlackKing().getX();
					}

					if (!fieldObject.checkBeat(yKing, xKing, getColor())) {

						setNotCheck();
//						checkMat();
						checkAllShapeOnCheckOpossite(yKing, xKing);
						step = new Step(true, brokenShape, from + "-" + to, getColor());
						fieldObject.getSteps().add(step);
						history.add(from + "-" + to);
					} else {
						LOGGER.error("Elephant can't step");
						field[yFrom][xFrom] = shapeFrom;
						field[yTo][xTo] = brokenShape;
						brokenShape = null;
						error = true;
					}
				}
			}
			// нет фигуры
			else if (field[yTo][xTo] == null) {

				while ((yTemp != yFrom) && (xTemp != xFrom)) {

					if (field[yTemp][xTemp] != null) {
						LOGGER.error("Elephant can't step");
						error = true;
						break;
					}
					if (defY > 0) {
						yTemp--;
					} else {
						yTemp++;
					}
					if (defX < 0) {
						xTemp++;
					} else {
						xTemp--;
					}

				}
				if (!error) {

					field[yFrom][xFrom] = null;
					field[yTo][xTo] = shapeFrom;

					int yKing = 0;
					int xKing = 0;
					if ("white".equals(getColor())) {
						yKing = fieldObject.getWhiteKing().getY();
						xKing = fieldObject.getWhiteKing().getX();
					} else if ("black".equals(getColor())) {
						yKing = fieldObject.getBlackKing().getY();
						xKing = fieldObject.getBlackKing().getX();
					}

					if (!fieldObject.checkBeat(yKing, xKing, getColor())) {
						setNotCheck();
						LOGGER.debug("Elephant end step");
//						checkMat();
						checkAllShapeOnCheckOpossite(yKing, xKing);
						step = new Step(true, null, from + "-" + to, getColor());
						fieldObject.getSteps().add(step);
						history.add(from + "-" + to);
					} else {
						LOGGER.error("Elephant can't step");
						field[yFrom][xFrom] = shapeFrom;
						field[yTo][xTo] = null;
						error = true;
					}
				}
			} else {
				error = true;
			}
		} else {
			error = true;
		}

		if (error) {
			step = new Step(false, null, "", getColor());
		}
		return step;
	}

	private void setNotCheck() {
		LOGGER.error("Elephant set not check");
		if ("white".equals(getColor())) {
			fieldObject.getWhiteKing().setCheck(false);
		} else if ("black".equals(getColor())) {
			fieldObject.getBlackKing().setCheck(false);
		}
	}

	public boolean checkBeatKing(int y, int x, int yKing, int xKing, Shape[][] shapes) {

		int defY = yKing - y;
		int defX = xKing - x;

		int yTemp = yKing;
		int xTemp = xKing;

		if (Math.abs(defY) == Math.abs(defX)) {

			while (((defY > 0 ? --yTemp : ++yTemp) != y && (defX < 0 ? ++xTemp : --xTemp) != x)) {

				if (shapes[yTemp][xTemp] != null) {
					LOGGER.error("Elephant can't beat king");
					return false;
				}
			}
			return true;
		}
		return false;
	}

	// проверка, после хода фигуры, не бъет ли союзная фигура короля
	private void checkAllShapeOnCheckOpossite(int yKing, int xKing) {
		String color = "";
		if ("white".equals(getColor())) {
			color = "black";
			yKing = fieldObject.getBlackKing().getY();
			xKing = fieldObject.getBlackKing().getX();
		} else if ("black".equals(getColor())) {
			color = "white";
			yKing = fieldObject.getWhiteKing().getY();
			xKing = fieldObject.getWhiteKing().getX();
		}

		if (fieldObject.checkBeat(yKing, xKing, color)) {
			if (!"white".equals(getColor())) {
				System.out.println("BEAT WHITE KING");
				fieldObject.getWhiteKing().setCheck(true);
			} else if (!"black".equals(getColor())) {
				System.out.println("BEAT BLACK KING");
				fieldObject.getBlackKing().setCheck(true);
			}
			
			if (((King) fieldObject.getField()[yKing][xKing]).isMat(yKing, xKing, fieldObject, yTo, xTo)) {
				if (!"white".equals(getColor())) {
					fieldObject.getWhiteKing().setMat(true);
				} else if (!"black".equals(getColor())) {
					fieldObject.getBlackKing().setMat(true);
				}
			}
		}
	}

	private void checkMat() {
		int yKing = 0;
		int xKing = 0;
		if (!"white".equals(getColor())) {
			yKing = fieldObject.getWhiteKing().getY();
			xKing = fieldObject.getWhiteKing().getX();
		} else if (!"black".equals(getColor())) {
			yKing = fieldObject.getBlackKing().getY();
			xKing = fieldObject.getBlackKing().getX();
		}

		if (this.checkBeatKing(yTo, xTo, yKing, xKing, fieldObject.getField())) {

			if (!"white".equals(getColor())) {
				System.out.println("BEAT WHITE KING");
				fieldObject.getWhiteKing().setCheck(true);
			} else if (!"black".equals(getColor())) {
				System.out.println("BEAT BLACK KING");
				fieldObject.getBlackKing().setCheck(true);
			}

			if (((King) fieldObject.getField()[yKing][xKing]).isMat(yKing, xKing, fieldObject, yTo, xTo)) {
				if (!"white".equals(getColor())) {
					fieldObject.getWhiteKing().setMat(true);
				} else if (!"black".equals(getColor())) {
					fieldObject.getBlackKing().setMat(true);
				}
			}
		}
	}

	@Override
	public boolean opportunityToClose(int yKing, int xKing, int y, int x) {
		int defY = y - yKing;
		int defX = x - xKing;

		int yTemp = y;
		int xTemp = x;
		
		while ((yTemp != yKing) && (xTemp != xKing)) {

			if(fieldObject.checkBlock(yTemp, xTemp, getColor())) {
				return true;
			}
			
			if (defY > 0) {
				yTemp--;
			} else {
				yTemp++;
			}
			if (defX < 0) {
				xTemp++;
			} else {
				xTemp--;
			}

		}
		return false;
	}

	@Override
	public boolean isCanStep(int y, int x, int yWhen, int xWhen, Shape[][] shapes) {
		
		int defY = yWhen - y;
		int defX = xWhen - x;

		int yTemp = yWhen;
		int xTemp = xWhen;
		
		if (Math.abs(defY) == Math.abs(defX)) {
			while (((defY > 0 ? --yTemp : ++yTemp) != y && (defX < 0 ? ++xTemp : --xTemp) != x)) {

				System.out.println("defY = "+ defY + " yTemp=" + yTemp + " xTemp = " + yTemp + " y = " + y + " x = " + x);
				
				if (shapes[yTemp][xTemp] != null) {
					return false;
				}
			}
			System.out.println("ELEPHANT IS BLOCK");
			return true;
		}
		return false;
	}
}
