package com.mark.chess.game.shape;

import java.util.LinkedList;

import org.apache.log4j.Logger;

import com.mark.chess.game.Field;

public class Turm extends Shape {
	
	private final static Logger LOGGER = Logger.getLogger(Turm.class);

	public Turm(String name, String color) {
		super(name, color);
	}

	private LinkedList<String> history;
	private Step step;
	private int xFrom;
	private int yFrom;
	private int xTo;
	private int yTo;
	private boolean firstStep = true;
	private Field fieldObject;

	@Override
	public Step doStep(Field fieldObject, String from, String to) {

		this.fieldObject = fieldObject;
		xFrom = Field.CHAR_MAP.get(from.charAt(0));
		yFrom = Integer.parseInt(String.valueOf(from.charAt(1))) - 1;

		xTo = Field.CHAR_MAP.get(to.charAt(0));
		yTo = Integer.parseInt(String.valueOf(to.charAt(1))) - 1;

		Shape[][] field = fieldObject.getField();

		history = fieldObject.getHistory();

		int defY = yTo - yFrom;
		int defX = xTo - xFrom;

		int yTemp = yTo;
		int xTemp = xTo;

		boolean error = false;

		Shape brokenShape = null;

		if (xTo == xFrom) {

			if (field[yTo][xTo] != null && !field[yTo][xTo].getColor().equals(field[yFrom][xFrom].getColor())) {

				while (((defY > 0 ? --yTemp : ++yTemp) != yFrom)) {

					if (field[yTemp][xTemp] != null) {
						error = true;
						break;
					}
				}

				if (!error) {
					brokenShape = field[yTo][xTo];
					field[yTo][xTo] = field[yFrom][xFrom];
					field[yFrom][xFrom] = null;
					
					int yKing = 0;
					int xKing = 0;
					if("white".equals(getColor())) {
						yKing = fieldObject.getWhiteKing().getY();
						xKing = fieldObject.getWhiteKing().getX();
					} else if ("black".equals(getColor())) {
						yKing = fieldObject.getBlackKing().getY();
						xKing = fieldObject.getBlackKing().getX();
					}
					
					if(!fieldObject.checkBeat(yKing, xKing, getColor())) {
						setNotCheck();
						firstStep = false;
						step = new Step(true, brokenShape, from + "-" + to, getColor());
						fieldObject.getSteps().add(step);
//						checkMat();
					} else {
						field[yFrom][xFrom] = field[yTo][xTo];
						field[yTo][xTo] = brokenShape;
						brokenShape = null;
						error = true;
					}
				}

			} else if (field[yTo][xTo] == null) {

				while (yTemp != yFrom) {

					if (field[yTemp][xTemp] != null) {
						error = true;
						break;
					}
					if (defY > 0) {
						yTemp--;
					} else {
						yTemp++;
					}
				}

				if (!error) {
					field[yTo][xTo] = field[yFrom][xFrom];
					field[yFrom][xFrom] = null;
					
					int yKing = 0;
					int xKing = 0;
					if("white".equals(getColor())) {
						yKing = fieldObject.getWhiteKing().getY();
						xKing = fieldObject.getWhiteKing().getX();
					} else if ("black".equals(getColor())) {
						yKing = fieldObject.getBlackKing().getY();
						xKing = fieldObject.getBlackKing().getX();
					}
					
					if(!fieldObject.checkBeat(yKing, xKing, getColor())) {
						setNotCheck();
						firstStep = false;
						step = new Step(true, brokenShape, from + "-" + to, getColor());
						fieldObject.getSteps().add(step);
//						checkMat();
					} else {
						field[yFrom][xFrom] = field[yTo][xTo];
						field[yTo][xTo] = null;
						error = true;
					}
					
				}
			} else {
				error = true;
			}
		} else if (yFrom == yTo) {

			if (field[yTo][xTo] != null && !field[yTo][xTo].getColor().equals(field[yFrom][xFrom].getColor())) {

				while (((defX > 0 ? --xTemp : ++xTemp) != xFrom)) {

					if (field[yTemp][xTemp] != null) {
						error = true;
						break;
					}
				}

				if (!error) {
					brokenShape = field[yTo][xTo];
					field[yTo][xTo] = field[yFrom][xFrom];
					field[yFrom][xFrom] = null;
					
					int yKing = 0;
					int xKing = 0;
					if("white".equals(getColor())) {
						yKing = fieldObject.getWhiteKing().getY();
						xKing = fieldObject.getWhiteKing().getX();
					} else if ("black".equals(getColor())) {
						yKing = fieldObject.getBlackKing().getY();
						xKing = fieldObject.getBlackKing().getX();
					}
					
					if(!fieldObject.checkBeat(yKing, xKing, getColor())) {
						setNotCheck();
						firstStep = false;
						step = new Step(true, brokenShape, from + "-" + to, getColor());
						fieldObject.getSteps().add(step);
//						checkMat();
					} else {
						error = true;
						field[yFrom][xFrom] = field[yTo][xTo];
						field[yTo][xTo] = brokenShape;
						brokenShape = null;
					}
					
				}

			} else if (field[yTo][xTo] == null) {

				while (xTemp != xFrom) {

					if (field[yTemp][xTemp] != null) {
						error = true;
						break;
					}
					if (defX < 0) {
						xTemp++;
					} else {
						xTemp--;
					}
				}

				if (!error) {
					field[yTo][xTo] = field[yFrom][xFrom];
					field[yFrom][xFrom] = null;
					
					int yKing = 0;
					int xKing = 0;
					if("white".equals(getColor())) {
						yKing = fieldObject.getWhiteKing().getY();
						xKing = fieldObject.getWhiteKing().getX();
					} else if ("black".equals(getColor())) {
						yKing = fieldObject.getBlackKing().getY();
						xKing = fieldObject.getBlackKing().getX();
					}
					
					if(!fieldObject.checkBeat(yKing, xKing, getColor())) {
						setNotCheck();
						firstStep = false;
						step = new Step(true, brokenShape, from + "-" + to, getColor());
						fieldObject.getSteps().add(step);
//						checkMat();
					} else {
						error = true;
						field[yFrom][xFrom] = field[yTo][xTo];
						field[yTo][xTo] = null;
					}
				}
			} else {
				error = true;
			}

		} else {
			error = true;
		}

		if (error) {
			step = new Step(false, null, from + "-" + to, getColor());
		}

		return step;
	}

	private void setNotCheck() {
		if("white".equals(getColor())) {
			fieldObject.getWhiteKing().setCheck(false);
		} else if ("black".equals(getColor())) {
			fieldObject.getBlackKing().setCheck(false);
		}
	}
	
	@Override
	public boolean isFirstStep() {
		return firstStep;
	}

	@Override
	public boolean checkBeatKing(int y, int x, int yKing, int xKing, Shape[][] shapes) {

		int defY = yKing - y;
		int defX = xKing - x;

		int yTemp = yKing;
		int xTemp = xKing;

		if (yKing == y) {

			while (((defX > 0 ? --xTemp : ++xTemp) != x)) {

				if (shapes[yTemp][xTemp] != null) {
					return false;
				}
			}
			return true;

		} else if (xKing == x) {
			while (((defY > 0 ? --yTemp : ++yTemp) != y)) {

				System.out.println(fieldObject);
				
				if (shapes[yTemp][xTemp] != null) {
					return false;
				}
			}
			return true;
		}
		return false;
	}
	
	private void checkMat() {
		int yKing = 0;
		int xKing = 0;
		if(!"white".equals(getColor())) {
			yKing = fieldObject.getWhiteKing().getY();
			xKing = fieldObject.getWhiteKing().getX();
		} else if(!"black".equals(getColor())) {
			yKing = fieldObject.getBlackKing().getY();
			xKing = fieldObject.getBlackKing().getX();
		}
		
		if(this.checkBeatKing(yTo, xTo, yKing, xKing, fieldObject.getField())) {
			
			if(!"white".equals(getColor())) {
				System.out.println("BEAT WHITE KING");
				fieldObject.getWhiteKing().setCheck(true);
			} else if(!"black".equals(getColor())) {
				System.out.println("BEAT BLACK KING");
				fieldObject.getBlackKing().setCheck(true);
			}
			
			if(((King)fieldObject.getField()[yKing][xKing]).isMat(yKing, xKing, fieldObject, yTo, xTo)) {
				if(!"white".equals(getColor())) {
					fieldObject.getWhiteKing().setMat(true);
				} else if(!"black".equals(getColor())) {
					fieldObject.getBlackKing().setMat(true);
				}
			}
		}
	}

	@Override
	public boolean opportunityToClose(int yKing, int xKing, int y, int x) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isCanStep(int i, int j, int yTemp, int xTemp, Shape[][] field) {
		// TODO Auto-generated method stub
		return false;
	}
}
