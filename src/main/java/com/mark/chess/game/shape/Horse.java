package com.mark.chess.game.shape;

import java.util.LinkedList;

import org.apache.log4j.Logger;

import com.mark.chess.game.Field;

public class Horse extends Shape {
	
	private final static Logger LOGGER = Logger.getLogger(Horse.class);

	public Horse(String name, String color) {
		super(name, color);
	}

	private LinkedList<String> history;
	private Step step;
	private int xFrom;
	private int yFrom;
	private int xTo;
	private int yTo;
	private Field fieldObject;

	@Override
	public Step doStep(Field fieldObject, String from, String to) {

		this.fieldObject = fieldObject;
		xFrom = Field.CHAR_MAP.get(from.charAt(0));
		yFrom = Integer.parseInt(String.valueOf(from.charAt(1))) - 1;

		xTo = Field.CHAR_MAP.get(to.charAt(0));
		yTo = Integer.parseInt(String.valueOf(to.charAt(1))) - 1;

		Shape[][] field = fieldObject.getField();

		history = fieldObject.getHistory();

		boolean error = false;

		Shape brokenShape = null;

		if ((yTo - yFrom == 2 && xTo - xFrom == 1) || (yTo - yFrom == 2 && xTo - xFrom == -1)
				|| (yTo - yFrom == -2 && xTo - xFrom == 1) || (yTo - yFrom == -2 && xTo - xFrom == -1)
				|| (yTo - yFrom == 1 && xTo - xFrom == 2) || (yTo - yFrom == 1 && xTo - xFrom == -2)
				|| (yTo - yFrom == -1 && xTo - xFrom == 2 || (yTo - yFrom == -1 && xTo - xFrom == -2))) {

			if (field[yTo][xTo] != null && !field[yTo][xTo].getColor().equals(field[yFrom][xFrom].getColor())) {

				brokenShape = field[yTo][xTo];
				field[yTo][xTo] = field[yFrom][xFrom];
				field[yFrom][xFrom] = null;

				int yKing = 0;
				int xKing = 0;
				if ("white".equals(getColor())) {
					yKing = fieldObject.getWhiteKing().getY();
					xKing = fieldObject.getWhiteKing().getX();
				} else if ("black".equals(getColor())) {
					yKing = fieldObject.getBlackKing().getY();
					xKing = fieldObject.getBlackKing().getX();
				}

				if (!fieldObject.checkBeat(yKing, xKing, getColor())) {
					setNotCheck();
					step = new Step(true, brokenShape, from + "-" + to, getColor());
					fieldObject.getSteps().add(step);
//					checkMat();
					checkAllShapeOnCheckOpossite(yKing, xKing);
				} else {
					field[yFrom][xFrom] = field[yTo][xTo];
					field[yTo][xTo] = brokenShape;
					brokenShape = null;
					error = true;
				}

			} else if (field[yTo][xTo] == null) {
				field[yTo][xTo] = field[yFrom][xFrom];
				field[yFrom][xFrom] = null;

				int yKing = 0;
				int xKing = 0;
				if ("white".equals(getColor())) {
					yKing = fieldObject.getWhiteKing().getY();
					xKing = fieldObject.getWhiteKing().getX();
				} else if ("black".equals(getColor())) {
					yKing = fieldObject.getBlackKing().getY();
					xKing = fieldObject.getBlackKing().getX();
				}

				if (!fieldObject.checkBeat(yKing, xKing, getColor())) {
					setNotCheck();
					step = new Step(true, null, from + "-" + to, getColor());
					fieldObject.getSteps().add(step);
//					checkMat();
					checkAllShapeOnCheckOpossite(yKing, xKing);
				} else {
					field[yFrom][xFrom] = field[yTo][xTo];
					field[yTo][xTo] = null;
					error = true;
				}
			} else {
				error = true;
			}

		} else {
			error = true;
		}

		if (error) {
			step = new Step(false, null, from + "-" + to, getColor());
		}

		return step;
	}

	// проверка, после хода фигуры, не бъет ли союзная фигура короля
	private void checkAllShapeOnCheckOpossite(int yKing, int xKing) {
		String color = "";
		if ("white".equals(getColor())) {
			color = "black";
			yKing = fieldObject.getBlackKing().getY();
			xKing = fieldObject.getBlackKing().getX();
		} else if ("black".equals(getColor())) {
			color = "white";
			yKing = fieldObject.getWhiteKing().getY();
			xKing = fieldObject.getWhiteKing().getX();
		}

		if (fieldObject.checkBeat(yKing, xKing, color)) {
			if (!"white".equals(getColor())) {
				System.out.println("BEAT WHITE KING");
				fieldObject.getWhiteKing().setCheck(true);
			} else if (!"black".equals(getColor())) {
				System.out.println("BEAT BLACK KING");
				fieldObject.getBlackKing().setCheck(true);
			}
		}
		// ////////////////////////////////////////////////////////////////////
	}

	private void setNotCheck() {
		if ("white".equals(getColor())) {
			fieldObject.getWhiteKing().setCheck(false);
		} else if ("black".equals(getColor())) {
			fieldObject.getBlackKing().setCheck(false);
		}
	}

	@Override
	public boolean checkBeatKing(int y, int x, int yKing, int xKing, Shape[][] shapes) {

		if ((yKing - y == 2 && xKing - x == 1) || (yKing - y == 2 && xKing - x == -1)
				|| (yKing - y == -2 && xKing - x == 1) || (yKing - y == -2 && xKing - x == -1)
				|| (yKing - y == 1 && xKing - x == 2) || (yKing - y == 1 && xKing - x == -2)
				|| (yKing - y == -1 && xKing - x == 2 || (yKing - y == -1 && xKing - x == -2))) {
			System.out.println("Horse is beat");
			System.out.println("yKing=" + yKing + " xKing = " + yKing+ " y = " + y + " x = " + x + " color="+getColor());
			
			return true;
		}
		return false;
	}

	private void checkMat() {
		int yKing = 0;
		int xKing = 0;
		if (!"white".equals(getColor())) {
			yKing = fieldObject.getWhiteKing().getY();
			xKing = fieldObject.getWhiteKing().getX();
		} else if (!"black".equals(getColor())) {
			yKing = fieldObject.getBlackKing().getY();
			xKing = fieldObject.getBlackKing().getX();
		}

		if (this.checkBeatKing(yTo, xTo, yKing, xKing, fieldObject.getField())) {

			if (!"white".equals(getColor())) {
				System.out.println("BEAT WHITE KING");
				fieldObject.getWhiteKing().setCheck(true);
			} else if (!"black".equals(getColor())) {
				System.out.println("BEAT BLACK KING");
				fieldObject.getBlackKing().setCheck(true);
			}

			if (((King) fieldObject.getField()[yKing][xKing]).isMat(yKing, xKing, fieldObject, yTo, xTo)) {
				if (!"white".equals(getColor())) {
					fieldObject.getWhiteKing().setMat(true);
				} else if (!"black".equals(getColor())) {
					fieldObject.getBlackKing().setMat(true);
				}
			}
		}
	}

	@Override
	public boolean opportunityToClose(int yKing, int xKing, int y, int x) {
		if (fieldObject.checkBlock(y, x, getColor())) {
			return true;
		}
		return false;
	}

	@Override
	public boolean isCanStep(int y, int x, int yTemp, int xTemp, Shape[][] field) {
		if ((yTemp - y == 2 && xTemp - x == 1) || (yTemp - y == 2 && xTemp - x == -1)
				|| (yTemp - y == -2 && xTemp - x == 1) || (yTemp - y == -2 && xTemp - x == -1)
				|| (yTemp - y == 1 && xTemp - x == 2) || (yTemp - y == 1 && xTemp - x == -2)
				|| (yTemp - y == -1 && xTemp - x == 2 || (yTemp - y == -1 && xTemp - x == -2))) {
			System.out.println("Horse is can");
			return true;
		}
		return false;
	}
}
