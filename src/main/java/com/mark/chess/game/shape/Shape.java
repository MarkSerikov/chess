package com.mark.chess.game.shape;

import com.mark.chess.game.Field;

public abstract class Shape {
	
	private String name;
	private String color;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	
	public Shape(String name, String color) {
		this.name = name;
		this.color = color;
	}
	
	public abstract Step doStep(Field field, String from, String to);
	
	public boolean isFirstStep() {
		return false;
	}
	public abstract boolean checkBeatKing(int i, int j, int yKing, int xKing, Shape[][] shapes);
	public abstract boolean opportunityToClose(int yKing, int xKing, int y, int x);
	public abstract boolean isCanStep(int i, int j, int yTemp, int xTemp, Shape[][] field);
}
