package com.mark.chess.game.shape;

import java.util.LinkedList;

import org.apache.log4j.Logger;

import com.mark.chess.game.Field;

public class King extends Shape {

	private final static Logger LOGGER = Logger.getLogger(King.class);

	public King(String name, String color) {
		super(name, color);
	}

	private LinkedList<String> history;
	private Step step;
	private int xFrom;
	private int yFrom;
	private int xTo;
	private int yTo;
	private boolean firstStep = true;
	private Field fieldObject;

	@Override
	public Step doStep(Field fieldObject, String from, String to) {

		this.fieldObject = fieldObject;
		xFrom = Field.CHAR_MAP.get(from.charAt(0));
		yFrom = Integer.parseInt(String.valueOf(from.charAt(1))) - 1;

		xTo = Field.CHAR_MAP.get(to.charAt(0));
		yTo = Integer.parseInt(String.valueOf(to.charAt(1))) - 1;

		Shape[][] field = fieldObject.getField();

		history = fieldObject.getHistory();

		int defY = yTo - yFrom;
		int defX = xTo - xFrom;

		int yTemp = yTo;
		int xTemp = xTo;

		boolean error = false;

		Shape brokenShape = null;

		System.out.println("!fieldObject.checkBeat(yTo, xTo, getColor()) = "
				+ !fieldObject.checkBeat(yTo, xTo, getColor()));

		if (xTo == xFrom && (Math.abs(yTo - yFrom) == 1) && !fieldObject.checkBeat(yTo, xTo, getColor())) {

			if (field[yTo][xTo] != null && !field[yTo][xTo].getColor().equals(field[yFrom][xFrom].getColor())) {

				while (((defY > 0 ? --yTemp : ++yTemp) != yFrom)) {

					if (field[yTemp][xTemp] != null) {
						error = true;
						break;
					}
				}

				if (!error) {
					setNotCheck();
					firstStep = false;
					brokenShape = field[yTo][xTo];
					field[yTo][xTo] = field[yFrom][xFrom];
					field[yFrom][xFrom] = null;
					checkAllShapeOnCheckOpossite();
					step = new Step(true, brokenShape, from + "-" + to, getColor());
					fieldObject.getSteps().add(step);

					if ("white".equals(getColor())) {
						fieldObject.getWhiteKing().setY(yTo);
						fieldObject.getWhiteKing().setX(xTo);
					} else if ("black".equals(getColor())) {
						fieldObject.getBlackKing().setY(yTo);
						fieldObject.getBlackKing().setX(xTo);
					}
				}

			} else if (field[yTo][xTo] == null) {

				while (yTemp != yFrom) {

					if (field[yTemp][xTemp] != null) {
						error = true;
						break;
					}
					if (defY > 0) {
						yTemp--;
					} else {
						yTemp++;
					}
				}

				if (!error) {
					setNotCheck();
					firstStep = false;
					field[yTo][xTo] = field[yFrom][xFrom];
					field[yFrom][xFrom] = null;
					checkAllShapeOnCheckOpossite();
					step = new Step(true, brokenShape, from + "-" + to, getColor());
					fieldObject.getSteps().add(step);

					if ("white".equals(getColor())) {
						fieldObject.getWhiteKing().setY(yTo);
						fieldObject.getWhiteKing().setX(xTo);
					} else if ("black".equals(getColor())) {
						fieldObject.getBlackKing().setY(yTo);
						fieldObject.getBlackKing().setX(xTo);
					}
				}
			} else {
				error = true;
			}
		} else if (yFrom == yTo && (Math.abs(xTo - xFrom) == 1)
				&& !fieldObject.checkBeat(yTo, xTo, getColor())) {

			if (field[yTo][xTo] != null && !field[yTo][xTo].getColor().equals(field[yFrom][xFrom].getColor())) {

				while (((defX > 0 ? --xTemp : ++xTemp) != xFrom)) {

					if (field[yTemp][xTemp] != null) {
						System.out.println("error");
						System.out.println("xTemp=" + xTemp);
						error = true;
						break;
					}
				}

				if (!error) {
					setNotCheck();
					firstStep = false;
					checkAllShapeOnCheckOpossite();
					brokenShape = field[yTo][xTo];
					field[yTo][xTo] = field[yFrom][xFrom];
					field[yFrom][xFrom] = null;
					step = new Step(true, brokenShape, from + "-" + to, getColor());
					fieldObject.getSteps().add(step);

					if ("white".equals(getColor())) {
						fieldObject.getWhiteKing().setY(yTo);
						fieldObject.getWhiteKing().setX(xTo);
					} else if ("black".equals(getColor())) {
						fieldObject.getBlackKing().setY(yTo);
						fieldObject.getBlackKing().setX(xTo);
					}
				}

			} else if (field[yTo][xTo] == null) {

				while (xTemp != xFrom) {

					if (field[yTemp][xTemp] != null) {
						error = true;
						break;
					}
					if (defX < 0) {
						xTemp++;
					} else {
						xTemp--;
					}
				}

				if (!error) {
					setNotCheck();
					checkAllShapeOnCheckOpossite();
					firstStep = false;
					field[yTo][xTo] = field[yFrom][xFrom];
					field[yFrom][xFrom] = null;
					step = new Step(true, brokenShape, from + "-" + to, getColor());
					fieldObject.getSteps().add(step);

					if ("white".equals(getColor())) {
						fieldObject.getWhiteKing().setY(yTo);
						fieldObject.getWhiteKing().setX(xTo);
					} else if ("black".equals(getColor())) {
						fieldObject.getBlackKing().setY(yTo);
						fieldObject.getBlackKing().setX(xTo);
					}
				}
			} else {
				error = true;
			}

		} else if (yFrom == yTo && xTo == 6 && field[yFrom][6] == null && field[yFrom][5] == null
				&& firstStep && field[yFrom][7].isFirstStep() && !fieldObject.checkBeat(yFrom, 4, getColor())
				&& !fieldObject.checkBeat(yFrom, 5, getColor()) && !fieldObject.checkBeat(yFrom, 6, getColor())) {
			firstStep = false;
			field[yFrom][6] = field[yFrom][xFrom];
			field[yFrom][xFrom] = null;
			field[yFrom][5] = field[yFrom][7];
			field[yFrom][7] = null;
			step = new Step(true, null, from + "-" + to, getColor());
			step.setRock(true);
			step.setPosition("right");
			System.out.println("RIGHT ROCK");
			fieldObject.getSteps().add(step);

			if ("white".equals(getColor())) {
				fieldObject.getWhiteKing().setY(yTo);
				fieldObject.getWhiteKing().setX(xTo);
			} else if ("black".equals(getColor())) {
				fieldObject.getBlackKing().setY(yTo);
				fieldObject.getBlackKing().setX(xTo);
			}

		} else if (yFrom == yTo && xTo == 2 && field[yFrom][3] == null && field[yFrom][2] == null
				&& field[yFrom][1] == null && firstStep && field[yFrom][0].isFirstStep()
				&& !fieldObject.checkBeat(yFrom, 2, getColor()) && !fieldObject.checkBeat(yFrom, 3, getColor())
				&& !fieldObject.checkBeat(yFrom, 4, getColor())) {
			firstStep = false;
			field[yFrom][2] = field[yFrom][xFrom];
			field[yFrom][xFrom] = null;
			field[yFrom][3] = field[yFrom][0];
			field[yFrom][0] = null;
			step = new Step(true, null, from + "-" + to, getColor());
			step.setRock(true);
			step.setPosition("left");
			
			System.out.println("LEFT ROCK");
			
			fieldObject.getSteps().add(step);

			if ("white".equals(getColor())) {
				fieldObject.getWhiteKing().setY(yTo);
				fieldObject.getWhiteKing().setX(xTo);
			} else if ("black".equals(getColor())) {
				fieldObject.getBlackKing().setY(yTo);
				fieldObject.getBlackKing().setX(xTo);
			}

		} else if (Math.abs(defY) == Math.abs(defX) && Math.abs(defY) == 1 && Math.abs(defX) == 1
				&& !fieldObject.checkBeat(yTo, xTo, getColor())) {

			// есть фигура
			if (field[yTo][xTo] != null && !field[yTo][xTo].getColor().equals(field[yFrom][xFrom].getColor())) {
				while (((defY > 0 ? --yTemp : ++yTemp) != yFrom) && (defX < 0 ? ++xTemp : --xTemp) != xFrom) {

					if (field[yTemp][xTemp] != null) {
						error = true;
						break;
					}
				}
				if (!error) {
					setNotCheck();
					checkAllShapeOnCheckOpossite();
					firstStep = false;
					brokenShape = field[yTo][xTo];
					field[yTo][xTo] = field[yFrom][xFrom];
					field[yFrom][xFrom] = null;
					step = new Step(true, brokenShape, from + "-" + to, getColor());
					fieldObject.getSteps().add(step);
					history.add(from + "-" + to);

					if ("white".equals(getColor())) {
						fieldObject.getWhiteKing().setY(yTo);
						fieldObject.getWhiteKing().setX(xTo);
					} else if ("black".equals(getColor())) {
						fieldObject.getBlackKing().setY(yTo);
						fieldObject.getBlackKing().setX(xTo);
					}
				}
			}
			// нет фигуры
			else if (field[yTo][xTo] == null) {

				while ((yTemp != yFrom) && (xTemp != xFrom)) {

					if (field[yTemp][xTemp] != null) {
						error = true;
						break;
					}
					if (defY > 0) {
						yTemp--;
					} else {
						yTemp++;
					}
					if (defX < 0) {
						xTemp++;
					} else {
						xTemp--;
					}

				}
				if (!error) {
					setNotCheck();
					checkAllShapeOnCheckOpossite();
					firstStep = false;
					field[yTo][xTo] = field[yFrom][xFrom];
					field[yFrom][xFrom] = null;
					step = new Step(true, null, from + "-" + to, getColor());
					fieldObject.getSteps().add(step);
					history.add(from + "-" + to);

					if ("white".equals(getColor())) {
						fieldObject.getWhiteKing().setY(yTo);
						fieldObject.getWhiteKing().setX(xTo);
					} else if ("black".equals(getColor())) {
						fieldObject.getBlackKing().setY(yTo);
						fieldObject.getBlackKing().setX(xTo);
					}
				}
			} else {
				error = true;
			}
		} else {
			error = true;
		}

		if (error) {
			step = new Step(false, null, from + "-" + to, getColor());
		}

		return step;
	}

	private void setNotCheck() {
		if ("white".equals(getColor())) {
			fieldObject.getWhiteKing().setCheck(false);
		} else if ("black".equals(getColor())) {
			fieldObject.getBlackKing().setCheck(false);
		}
	}

	// проверка, после хода фигуры, не бъет ли союзная фигура короля
	private void checkAllShapeOnCheckOpossite() {

		String color = "";
		int yKing = 0;
		int xKing = 0;
		if ("white".equals(getColor())) {
			color = "black";
			yKing = fieldObject.getBlackKing().getY();
			xKing = fieldObject.getBlackKing().getX();
		} else if ("black".equals(getColor())) {
			color = "white";
			yKing = fieldObject.getWhiteKing().getY();
			xKing = fieldObject.getWhiteKing().getX();
		}

		if (fieldObject.checkBeat(yKing, xKing, color)) {
			if (!"white".equals(getColor())) {
				System.out.println("BEAT WHITE KING");
				fieldObject.getWhiteKing().setCheck(true);
			} else if (!"black".equals(getColor())) {
				System.out.println("BEAT BLACK KING");
				fieldObject.getBlackKing().setCheck(true);
			}
		}
	}

	@Override
	public boolean checkBeatKing(int y, int x, int yKing, int xKing, Shape[][] shapes) {

		if ((Math.abs(yKing - y) == 1 && Math.abs(xKing - x) < 2)
				|| (Math.abs(xKing - x) == 1 && Math.abs(yKing - y) < 2)) {
			return true;
		}
		return false;
	}

	public boolean isMat(int yKing, int xKing, Field fieldObject, int y, int x) {

		this.fieldObject = fieldObject;

		if (yKing == 7 && xKing == 0) {
			if (!fieldObject.checkBeat(yKing, xKing + 1, getColor())) {
				if (fieldObject.getField()[yKing][xKing + 1] == null) {
					return false;
				}
			}
			if (!fieldObject.checkBeat(yKing - 1, xKing, getColor())) {
				if (fieldObject.getField()[yKing - 1][xKing] == null) {
					return false;
				}
			}
			if (!fieldObject.checkBeat(yKing - 1, xKing + 1, getColor())) {
				if (fieldObject.getField()[yKing - 1][xKing + 1] == null) {
					return false;
				}
			}
			if (fieldObject.getField()[y][x].opportunityToClose(yKing, xKing, y, x)) {
				return false;
			}
			setMat();
			return true;
		} else if (yKing == 7 && xKing == 7) {
			if (!fieldObject.checkBeat(yKing, xKing - 1, getColor())) {
				if (fieldObject.getField()[yKing][xKing - 1] == null) {
					return false;
				}
			}
			if (!fieldObject.checkBeat(yKing - 1, xKing, getColor())) {
				if (fieldObject.getField()[yKing - 1][xKing] == null) {
					return false;
				}
			}
			if (!fieldObject.checkBeat(yKing - 1, xKing - 1, getColor())) {
				if (fieldObject.getField()[yKing - 1][xKing - 1] == null) {
					return false;
				}
			}
			if (fieldObject.getField()[y][x].opportunityToClose(yKing, xKing, y, x)) {
				return false;
			}
			setMat();
			return true;
		} else if (yKing == 0 && xKing == 0) {
			if (!fieldObject.checkBeat(yKing, xKing + 1, getColor())) {
				if (fieldObject.getField()[yKing][xKing + 1] == null) {
					return false;
				}
			}
			if (!fieldObject.checkBeat(yKing + 1, xKing, getColor())) {
				if (fieldObject.getField()[yKing + 1][xKing] == null) {
					return false;
				}
			}
			if (!fieldObject.checkBeat(yKing + 1, xKing + 1, getColor())) {
				if (fieldObject.getField()[yKing + 1][xKing + 1] == null) {
					return false;
				}
			}
			if (fieldObject.getField()[y][x].opportunityToClose(yKing, xKing, y, x)) {
				return false;
			}
			setMat();
			return true;
		} else if (yKing == 0 && xKing == 7) {
			if (!fieldObject.checkBeat(yKing, xKing - 1, getColor())) {
				if (fieldObject.getField()[yKing][xKing - 1] == null) {
					return false;
				}
			}
			if (!fieldObject.checkBeat(yKing + 1, xKing, getColor())) {
				if (fieldObject.getField()[yKing + 1][xKing] == null) {
					return false;
				}
			}
			if (!fieldObject.checkBeat(yKing + 1, xKing - 1, getColor())) {
				if (fieldObject.getField()[yKing + 1][xKing - 1] == null) {
					return false;
				}
			}
			if (fieldObject.getField()[y][x].opportunityToClose(yKing, xKing, y, x)) {
				return false;
			}
			setMat();
			return true;
		} else if (xKing == 0 && yKing > 0 && yKing < 7) {
			if (!fieldObject.checkBeat(yKing, xKing + 1, getColor())) {
				if (fieldObject.getField()[yKing][xKing + 1] == null) {
					return false;
				}
			}
			if (!fieldObject.checkBeat(yKing + 1, xKing, getColor())) {
				if (fieldObject.getField()[yKing + 1][xKing] == null) {
					return false;
				}
			}
			if (!fieldObject.checkBeat(yKing + 1, xKing + 1, getColor())) {
				if (fieldObject.getField()[yKing + 1][xKing + 1] == null) {
					return false;
				}
			}
			if (!fieldObject.checkBeat(yKing - 1, xKing, getColor())) {
				if (fieldObject.getField()[yKing - 1][xKing] == null) {
					return false;
				}
			}
			if (!fieldObject.checkBeat(yKing - 1, xKing + 1, getColor())) {
				if (fieldObject.getField()[yKing - 1][xKing + 1] == null) {
					return false;
				}
			}
			if (fieldObject.getField()[y][x].opportunityToClose(yKing, xKing, y, x)) {
				return false;
			}
			setMat();
			return true;
		} else if (xKing == 7 && yKing > 0 && yKing < 7) {
			if (!fieldObject.checkBeat(yKing, xKing - 1, getColor())) {
				if (fieldObject.getField()[yKing][xKing - 1] == null) {
					return false;
				}
			}
			if (!fieldObject.checkBeat(yKing + 1, xKing, getColor())) {
				if (fieldObject.getField()[yKing + 1][xKing] == null) {
					return false;
				}
			}
			if (!fieldObject.checkBeat(yKing + 1, xKing - 1, getColor())) {
				if (fieldObject.getField()[yKing + 1][xKing - 1] == null) {
					return false;
				}
			}
			if (!fieldObject.checkBeat(yKing - 1, xKing, getColor())) {
				if (fieldObject.getField()[yKing - 1][xKing] == null) {
					return false;
				}
			}
			if (!fieldObject.checkBeat(yKing - 1, xKing - 1, getColor())) {
				if (fieldObject.getField()[yKing - 1][xKing - 1] == null) {
					return false;
				}
			}
			if (fieldObject.getField()[y][x].opportunityToClose(yKing, xKing, y, x)) {
				return false;
			}
			setMat();
			return true;
		} else if (yKing == 7 && xKing > 0 && xKing < 7) {
			if (!fieldObject.checkBeat(yKing, xKing + 1, getColor())) {
				if (fieldObject.getField()[yKing][xKing + 1] == null) {
					return false;
				}
			}
			if (!fieldObject.checkBeat(yKing, xKing - 1, getColor())) {
				if (fieldObject.getField()[yKing][xKing - 1] == null) {
					return false;
				}
			}
			if (!fieldObject.checkBeat(yKing - 1, xKing + 1, getColor())) {
				if (fieldObject.getField()[yKing - 1][xKing + 1] == null) {
					return false;
				}
			}
			if (!fieldObject.checkBeat(yKing - 1, xKing, getColor())) {
				if (fieldObject.getField()[yKing - 1][xKing] == null) {
					return false;
				}
			}
			if (!fieldObject.checkBeat(yKing - 1, xKing - 1, getColor())) {
				if (fieldObject.getField()[yKing - 1][xKing - 1] == null) {
					return false;
				}
			}
			if (fieldObject.getField()[y][x].opportunityToClose(yKing, xKing, y, x)) {
				return false;
			}
			setMat();
			return true;
		} else if (yKing == 0 && xKing > 0 && xKing < 7) {
			if (!fieldObject.checkBeat(yKing, xKing + 1, getColor())) {
				if (fieldObject.getField()[yKing][xKing + 1] == null) {
					return false;
				}
			}
			if (!fieldObject.checkBeat(yKing, xKing - 1, getColor())) {
				if (fieldObject.getField()[yKing][xKing - 1] == null) {
					return false;
				}
			}
			if (!fieldObject.checkBeat(yKing + 1, xKing + 1, getColor())) {
				if (fieldObject.getField()[yKing + 1][xKing + 1] == null) {
					return false;
				}
			}
			if (!fieldObject.checkBeat(yKing + 1, xKing, getColor())) {
				if (fieldObject.getField()[yKing + 1][xKing] == null) {
					return false;
				}
			}
			if (!fieldObject.checkBeat(yKing + 1, xKing - 1, getColor())) {
				if (fieldObject.getField()[yKing + 1][xKing - 1] == null) {
					return false;
				}
			}
			if (fieldObject.getField()[y][x].opportunityToClose(yKing, xKing, y, x)) {
				return false;
			}
			setMat();
			return true;
		} else if (yKing > 0 && yKing < 7 && xKing > 0 && xKing < 7) {
			if (!fieldObject.checkBeat(yKing, xKing + 1, getColor())) {
				if (fieldObject.getField()[yKing][xKing + 1] == null) {
					return false;
				}
			}
			if (!fieldObject.checkBeat(yKing, xKing - 1, getColor())) {
				if (fieldObject.getField()[yKing][xKing - 1] == null) {
					return false;
				}
			}
			if (!fieldObject.checkBeat(yKing + 1, xKing + 1, getColor())) {
				if (fieldObject.getField()[yKing + 1][xKing + 1] == null) {
					return false;
				}
			}
			if (!fieldObject.checkBeat(yKing + 1, xKing, getColor())) {
				if (fieldObject.getField()[yKing + 1][xKing] == null) {
					return false;
				}
			}
			if (!fieldObject.checkBeat(yKing + 1, xKing - 1, getColor())) {
				if (fieldObject.getField()[yKing + 1][xKing - 1] == null) {
					return false;
				}
			}
			if (!fieldObject.checkBeat(yKing - 1, xKing + 1, getColor())) {
				if (fieldObject.getField()[yKing - 1][xKing + 1] == null) {
					return false;
				}
			}
			if (!fieldObject.checkBeat(yKing - 1, xKing, getColor())) {
				if (fieldObject.getField()[yKing - 1][xKing] == null) {
					return false;
				}
			}
			if (!fieldObject.checkBeat(yKing - 1, xKing - 1, getColor())) {
				if (fieldObject.getField()[yKing - 1][xKing - 1] == null) {
					return false;
				}
			}
			if (fieldObject.getField()[y][x].opportunityToClose(yKing, xKing, y, x)) {
				return false;
			}
			setMat();
			return true;
		}
		return false;
	}

	private void setMat() {
		if ("white".equals(getColor())) {
			fieldObject.getWhiteKing().setMat(true);
		} else if ("black".equals(getColor())) {
			fieldObject.getBlackKing().setMat(true);
		}
	}

	@Override
	public boolean opportunityToClose(int yKing, int xKing, int y, int x) {
		return false;
	}

	@Override
	public boolean isCanStep(int i, int j, int yTemp, int xTemp, Shape[][] field) {
		return false;
	}
}
