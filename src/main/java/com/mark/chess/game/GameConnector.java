package com.mark.chess.game;

import java.util.Queue;

import org.apache.log4j.Logger;

import com.mark.chess.model.User;

public class GameConnector {
	private static final Logger LOGGER = Logger.getLogger(GameConnector.class);
	
	private Gamer gamer1;
	private Gamer gamer2;
	
	private boolean first = false;
	public boolean isFirst() {
		return first;
	}

	public Game connect(Gamer gamer1) {

		this.gamer1 = gamer1;
		
		Game game = null;
		boolean gameIs = false;

		Queue<Gamer> gamers = GameQueue.getInstance();
		
//		synchronized (gamers) {
			for (Gamer gamer : gamers) {
				if (!gamer1.getColor().equals(gamer.getColor())) {
					
					this.gamer2 = gamer;
					
					first = true;
					
					GameQueue.getInstance().remove(gamer);
					game = new Game(gamer1, gamer);
					gamer.setGame(game);
					gamer1.setGame(game);
					synchronized (gamer) {
						gamer.notify();
					}
					
					GamesList.getInstance().add(game);
					gameIs = true;
					break;
				}
			}
//		}

		System.out.println("before");
		if (!gameIs) {
			synchronized (gamer1) {
				GameQueue.getInstance().offer(gamer1);
				try {
					gamer1.wait();
					GameQueue.getInstance().remove(gamer1);
					game = gamer1.getGame();
					
				} catch (InterruptedException e) {
					LOGGER.error("Не удалось вывести поток из режима ожидания", e);
				}
			}
		}
		
		Field field = new Field(hoWhite(), hoBlack());
		game.setField(field);
		return game;
	}

	private User hoBlack() {
		if("black".equals(gamer1.getColor())) {
			return gamer1.getUser();
		} else {
			return gamer2.getUser();
		}

	}

	private User hoWhite() {
		if("black".equals(gamer1.getColor())) {
			return gamer1.getUser();
		} else {
			return gamer2.getUser();
		}
	}
}
