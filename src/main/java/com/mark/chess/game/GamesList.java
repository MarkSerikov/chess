package com.mark.chess.game;

import java.util.LinkedList;
import java.util.List;

public class GamesList {
	
	private GamesList() {}
	
	private static volatile List<Game> games;

	public static List<Game> getInstance() {
		if(games == null) {
			synchronized (GamesList.class) {
				if(games == null) {
					games = new LinkedList<Game>();
				}
			}
		}
		return games;
	}

}
