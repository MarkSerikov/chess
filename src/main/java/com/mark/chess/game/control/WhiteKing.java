package com.mark.chess.game.control;

public class WhiteKing {
	
	private int y;
	private int x;
	private boolean check;
	private boolean mat;
	public WhiteKing(int y, int x) {
		this.y = y;
		this.x = x;
	}
	public int getY() {
		return y;
	}
	public void setY(int y) {
		this.y = y;
	}
	public int getX() {
		return x;
	}
	public void setX(int x) {
		this.x = x;
	}
	public boolean isCheck() {
		return check;
	}
	public void setCheck(boolean check) {
		this.check = check;
	}
	public boolean isMat() {
		return mat;
	}
	public void setMat(boolean mat) {
		this.mat = mat;
	}
}
