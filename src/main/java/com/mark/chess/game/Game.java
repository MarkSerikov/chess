package com.mark.chess.game;

public class Game {

	private String gameName;
	private final Gamer gamer1;
	private final Gamer gamer2;
	Field field;

	public String getGameName() {
		return gameName;
	}
	public void setGameName(String gameName) {
		this.gameName = gameName;
	}
	public Gamer getGamer1() {
		return gamer1;
	}
	public Gamer getGamer2() {
		return gamer2;
	}
	public Field getField() {
		return field;
	}
	public void setField(Field field) {
		this.field = field;
	}
	
	public Game(Gamer gamer1, Gamer gamer2) {
		this.gamer1 = gamer1;
		this.gamer2 = gamer2;
	}

}
