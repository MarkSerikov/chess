package com.mark.chess.game;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import com.mark.chess.game.control.BlackKing;
import com.mark.chess.game.control.Check;
import com.mark.chess.game.control.WhiteKing;
import com.mark.chess.game.shape.Elephant;
import com.mark.chess.game.shape.Horse;
import com.mark.chess.game.shape.King;
import com.mark.chess.game.shape.Pawn;
import com.mark.chess.game.shape.Queen;
import com.mark.chess.game.shape.Shape;
import com.mark.chess.game.shape.Step;
import com.mark.chess.game.shape.Turm;
import com.mark.chess.model.User;

public class Field {

	private final User whiteUser;
	private final User blackUser;
	
	private Check check;
	private WhiteKing whiteKing = new WhiteKing(0, 4);
	private BlackKing blackKing = new BlackKing(7, 4);

	private User hoDoStep;

	LinkedList<String> history = new LinkedList<String>();
	LinkedList<Step> steps = new LinkedList<Step>();

	private final Shape[][] field = new Shape[8][8];

	public final static Map<Character, Integer> CHAR_MAP = new HashMap<Character, Integer>();

	static {
		CHAR_MAP.put('A', 0);
		CHAR_MAP.put('B', 1);
		CHAR_MAP.put('C', 2);
		CHAR_MAP.put('D', 3);
		CHAR_MAP.put('E', 4);
		CHAR_MAP.put('F', 5);
		CHAR_MAP.put('G', 6);
		CHAR_MAP.put('H', 7);
	}
	
	
	public LinkedList<String> getHistory() {
		return history;
	}
	public Shape[][] getField() {
		return field;
	}
	public WhiteKing getWhiteKing() {
		return whiteKing;
	}
	public BlackKing getBlackKing() {
		return blackKing;
	}

	public Field(User whiteUser, User blackUser) {
		this.whiteUser = whiteUser;
		this.blackUser = blackUser;
		steps.add(new Step(false, null, "", ""));

		field[0][0] = new Turm("turm", "white");
		field[0][1] = new Horse("horse", "white");
		field[0][2] = new Elephant("elephant", "white");
		field[0][3] = new Queen("queen", "white");
		field[0][4] = new King("king", "white");
		field[0][5] = new Elephant("elephant", "white");
		field[0][6] = new Horse("horse", "white");
		field[0][7] = new Turm("turm", "white");

		for (int j = 0; j < field.length; j++) {
			field[1][j] = new Pawn("pawn", "white");

		}

		for (int i = 2; i < field.length - 2; i++) {
			for (int j = 0; j < field.length; j++) {
				field[i][j] = null;
			}
		}

		for (int j = 0; j < field.length; j++) {
			field[6][j] = new Pawn("pawn", "black");
		}

		field[7][0] = new Turm("turm", "black");
		field[7][1] = new Horse("horse", "black");
		field[7][2] = new Elephant("elephant", "black");
		field[7][3] = new Queen("queen", "black");
		field[7][4] = new King("king", "black");
		field[7][5] = new Elephant("elephant", "black");
		field[7][6] = new Horse("horse", "black");
		field[7][7] = new Turm("turm", "black");
	}

	public Step doStep(User user, String from, String to) {

		int xFrom = CHAR_MAP.get(from.charAt(0));
		int yFrom = Integer.parseInt(String.valueOf(from.charAt(1)));
		
		Shape shapeFrom = field[yFrom-1][xFrom];
		
		Step step = shapeFrom.doStep(this, from, to);

		System.out.println();
		System.out.println(step.isSuccess());
		System.out.println();
		
		return step;
	}
	
	public boolean checkBeat(int yKing, int xKing, String color) {
		
		for (int i = 0; i < field.length; i++) {
			for (int j = 0; j < field.length; j++) {
				if(field[i][j] != null && !field[i][j].getColor().equals(color)) {
					Shape shape = field[i][j];
					
					boolean beat = shape.checkBeatKing(i, j, yKing, xKing, field);
					if(beat) {
						System.out.println("Фигура, которая может бить короля");
						System.out.println("yKing=" + yKing + " xKing = " + yKing+ " y = " + i + " x = " + j + " color="+color);
						System.out.println("field[i][j] = " + field[i][j].getName() + field[i][j].getColor());
						return true;
					}
				}
			}
		}
		return false;
	}

	public boolean checkBlock(int yWhen, int xWhen, String color) {
		for (int i = 0; i < field.length; i++) {
			for (int j = 0; j < field.length; j++) {
				if(field[i][j] != null && !field[i][j].getColor().equals(color)) {
					Shape shape = field[i][j];
					boolean beat = shape.isCanStep(i, j, yWhen, xWhen, field);
					
//					System.out.println("field[i][j] = " + field[i][j].getName() + " "  + field[i][j].getColor() + " abil = " + beat);
					
					if(beat) {
						return true;
					}
				}
			}
		}
		return false;
	}
	
	public Step getLastStep() {
		return steps.getLast();
	}
	public LinkedList<Step> getSteps() {
		return steps;
	}
	public void setСheck(boolean b, String color) {
		check.setCheck(b);
		check.setColor(color);
	}
	public void setMat(boolean b, String string) {
		
	}
	
}
