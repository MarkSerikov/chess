package com.mark.chess.controller;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Locale;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.mark.chess.controller.parser.JsonRequestParser;
import com.mark.chess.game.Game;
import com.mark.chess.game.Field;
import com.mark.chess.game.GameConnector;
import com.mark.chess.game.GameForGamer;
import com.mark.chess.game.Gamer;
import com.mark.chess.game.shape.Step;
import com.mark.chess.model.User;
import com.mark.chess.service.IUserService;

@Controller
public class GameController {

	private static final Logger LOGGER = Logger.getLogger(GameController.class);

	private GameForGamer gameForGamer;
	private Step step = new Step(false, null, "", "");

	@Autowired
	private IUserService userService;

	private String whoStep = "white";

	@RequestMapping(value = "/step", method = RequestMethod.POST, produces = "text/plain;charset=UTF-8")
	public @ResponseBody
	String step(Locale locale, @RequestParam("user") String userString, @RequestParam("from") String from,
			@RequestParam("to") String to, String color) {

		if (whoStep.equals(color)) {
			User user = JsonRequestParser.getUserFromParam(userString);

			Step step = gameForGamer.getGame().getField().doStep(user, from, to);

			Step clearStep = new Step(step.isSuccess(), null, step.getStep(), step.getColor());
			clearStep.setRock(step.isRock());
			clearStep.setPosition(step.getPosition());
			clearStep.setCorner(step.isCorner());
			clearStep.setCord(step.getCord());
			if(step.isRock()) {
				clearStep.setRock(true);
			}
			if (clearStep.isSuccess()) {
				if ("white".equals(color)) {
					whoStep = "black";
				} else {
					whoStep = "white";
				}
			}
			return new Gson().toJson(clearStep);
		}
		return new Gson().toJson(new Step(false, null, null, null));

	}

	@RequestMapping(value = "/findStep", method = RequestMethod.POST, produces = "text/plain;charset=UTF-8")
	public @ResponseBody
	String findStep(Locale locale, String color) {

		if (gameForGamer != null) {
			Game game = gameForGamer.getGame();

			if (game != null) {
				Field field = gameForGamer.getGame().getField();
				Step step = field.getLastStep();
				if (this.step.getStep().equals(step.getStep()) || !whoStep.equals(color)) {
					return null;
				}
				this.step = step;
				Step stepClear = new Step(step.isSuccess(), null, step.getStep(), step.getColor());
				stepClear.setPosition(step.getPosition());
				stepClear.setCorner(step.isCorner());
				stepClear.setCord(step.getCord());
				if(step.isRock()) {
					stepClear.setRock(step.isRock());
				}
				
				if("white".equals(color)) {
					if(gameForGamer.getGame().getField().getWhiteKing().isCheck()) {
						stepClear.setCheck(true);
					}
					if(gameForGamer.getGame().getField().getWhiteKing().isMat()) {
						stepClear.setMat(true);
					}
				}
				if("black".equals(color)) {
					if(gameForGamer.getGame().getField().getBlackKing().isCheck()) {
						stepClear.setCheck(true);
					}
					if(gameForGamer.getGame().getField().getBlackKing().isMat()) {
						stepClear.setMat(true);
					}
				}
				return new Gson().toJson(stepClear);
			}

		}
		return null;

	}

	@RequestMapping(value = "/createGame", method = RequestMethod.POST, produces = "text/plain;charset=UTF-8")
	public String createGame(Locale locale, Model model, @RequestParam("user") String userString,
			@RequestParam("color") String color) {

		Map<Integer, Character> whiteMap = new HashMap<Integer, Character>();
		whiteMap.put(2, 'A');
		whiteMap.put(3, 'B');
		whiteMap.put(4, 'C');
		whiteMap.put(5, 'D');
		whiteMap.put(6, 'E');
		whiteMap.put(7, 'F');
		whiteMap.put(8, 'G');
		whiteMap.put(9, 'H');

		Map<Integer, Character> blackMap = new HashMap<Integer, Character>();
		blackMap.put(9, 'A');
		blackMap.put(8, 'B');
		blackMap.put(7, 'C');
		blackMap.put(6, 'D');
		blackMap.put(5, 'E');
		blackMap.put(4, 'F');
		blackMap.put(3, 'G');
		blackMap.put(2, 'H');

		LOGGER.info("Game starting");
		// User user = JsonRequestParser.getUserFromParam(userString);
		User user = new Gson().fromJson(userString, User.class);
		Gamer gamer = new Gamer(user, color);
		Game game = new GameConnector().connect(gamer);

		game.getGamer1().setGame(null);
		game.getGamer2().setGame(null);

		gameForGamer = new GameForGamer();
		gameForGamer.setGame(game);
		gameForGamer.setColor(color);
		model.addAttribute("game", gameForGamer);
		model.addAttribute("color", color);
		model.addAttribute("charMapWhite", whiteMap);
		model.addAttribute("charMapBlack", blackMap);
		return "game";
	}

	@RequestMapping(value = "/info", method = RequestMethod.POST, produces = "text/plain;charset=UTF-8")
	public @ResponseBody
	String getInfo(Locale locale) {

		String info = "Сайт разработан для курсовой работы по предмету спец языки программирования. В целях защиты курсовой работы. Автор: Сериков Марк Евгениевич.";
		return info;
	}
}
