package com.mark.chess.controller.parser;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import net.sf.json.JSONObject;

import com.mark.chess.model.User;

public class JsonRequestParser {
	
	private final static Logger LOGGER = Logger.getLogger(JsonRequestParser.class);

	public static User getUserFromParam(String stringJson) {
		
		User user = null;
		JSONObject jsonObject = null;

		String login = null;
		String idString = null;
		String password = null;
		Long id = 0L;

		try {
			
			jsonObject = JSONObject.fromObject(stringJson);

			Object idObject = jsonObject.get("id");
			Object nameObject = jsonObject.get("login");
			Object passwordObject = jsonObject.get("password");
			
			if (idObject != null && nameObject != null && passwordObject != null) {

				idString = idObject.toString();
				login = (String) nameObject;
				password = (String) passwordObject;
				
				System.out.println(idString);
				System.out.println(login);
				System.out.println(password);
				
				id = Long.parseLong(idString);

				if (id.longValue() != 0 && !StringUtils.isNotEmpty(login) && StringUtils.isNotEmpty(password)) {
					
					user = new User();
					user.setId(id);
					user.setName(login);
					user.setPassword(password);
				}
			}
		} catch (Exception e) {
			LOGGER.error("Не возможно получить пользователя из запроса", e);
		}
		return user;
	}

	public static String getColor(String colorString) {
		// TODO Auto-generated method stub
		return null;
	}

}
