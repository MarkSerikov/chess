package com.mark.chess.controller;

import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.mark.chess.model.User;
import com.mark.chess.service.IUserService;

@Controller
public class UserController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);
	
	@Autowired
	private IUserService userService;
	
	@RequestMapping(value = "/", method = RequestMethod.GET, produces = "text/plain;charset=UTF-8")
	public String getAutorizedPage(Locale locale, Model model) {
		LOGGER.info("User come into site");
		return "index";
	}
	
	@RequestMapping(value = "/registration", method = RequestMethod.POST, produces = "text/plain;charset=UTF-8")
	public @ResponseBody
	String registration(Locale locale, @RequestParam("user") String userString) {
		LOGGER.info("User want to registration");
		
		Gson gson = new Gson();
		
		//TODO сделать валидацию
		User user = gson.fromJson(userString, User.class);
		
		long id = userService.registration(user);
		user.setId(id);
		
		return new Gson().toJson(user); 
		
	}
	
	@RequestMapping(value = "/autorisation", method = RequestMethod.POST, produces = "text/plain;charset=UTF-8")
	public @ResponseBody String autorisation(Locale locale, @RequestParam("user") String userString) {
		LOGGER.info("User want autorisation");
		
		Gson gson = new Gson();
		//TODO сделать валидацию
		User user = gson.fromJson(userString, User.class);
		long id = userService.autorise(user);
		if(id > 0) {
			user.setId(id);
			return new Gson().toJson(user);
		}
		user.setId(id);
		user.setName("Mark");
		user.setPassword("pws");
		
		return new Gson().toJson(user);
	}
}
